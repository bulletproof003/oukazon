import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicApp, IonicModule, IonicErrorHandler, IonicPageModule } from 'ionic-angular';
import { HttpModule } from '@angular/http'
import { HttpClientModule } from '@angular/common/http';
import { SMS } from '@ionic-native/sms';
import { CallNumber } from '@ionic-native/call-number';
import { Oukazon } from './app.component';
import { PostyourPage } from '../pages/postyour/postyour';
import { LocalNotifications } from '@ionic-native/local-notifications';

import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

import { TimeAgoPipe } from 'time-ago-pipe';
import { IonicStorageModule } from '@ionic/storage';

import { AuthService } from '../providers/auth-service/auth-service';
import { Common } from '../providers/common';
import { ProductService } from '../providers/product-service';

import { TabsPage } from '../pages/tabs/tabs';
import { ClassifiedPage } from '../pages/classified/classified';
import { DirectoryPage } from '../pages/directory/directory';
import { AskPage } from '../pages/ask/ask';
import { HomePage } from '../pages/home/home';
import { SmodalPage } from '../pages/smodal/smodal';
import { AdDetailPage } from '../pages/ad-detail/ad-detail';
import { AdcatPage } from '../pages/adcat/adcat';
import { AdsubcatPage } from '../pages/adsubcat/adsubcat';
import { CompDetailPage } from '../pages/comp-detail/comp-detail';
import { CompcatPage } from '../pages/compcat/compcat';
import { CompsubcatPage } from '../pages/compsubcat/compsubcat';
import { AskDetailPage } from '../pages/ask-detail/ask-detail';
import { MapAutocompletePage } from '../pages/map-autocomplete/map-autocomplete';

import { PostAdPage } from '../pages/post-ad/post-ad';
import { EditAdPage } from '../pages/edit-ad/edit-ad';
import { PostCompPage } from '../pages/post-comp/post-comp';
import { EditCompPage } from '../pages/edit-comp/edit-comp';
import { PostAskPage } from '../pages/post-ask/post-ask';
import { EditAskPage } from '../pages/edit-ask/edit-ask';

import { LoginPage } from '../pages/login/login';
import { ClangPage } from '../pages/clang/clang';
import { SignupPage } from '../pages/signup/signup';
import { WelcomePage } from '../pages/welcome/welcome';
import { ProfilePage } from '../pages/profile/profile';
import { SettingsPage } from '../pages/settings/settings';
import { MessagesPage } from '../pages/messages/messages';
import { ChatModule } from '../pages/chat/chat.module';

import { MenuPageModule } from '../pages/menu/menu.module';
import { ParallaxDirective } from '../directives/parallax/parallax';
import { LinkyModule } from 'angular-linky';
import { MomentModule } from 'angular2-moment';

import { IonicImageViewerModule } from 'ionic-img-viewer';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { MapProvider } from '../providers/map/map';
import { SpinnerProvider } from '../providers/spinner/spinner';
import { SelectSearchableModule } from '../components/select/select-module';
import { EmojiPickerComponentModule } from '../components/emoji-picker/emoji-picker.module';
import { PayPal } from '@ionic-native/paypal';
import { EmojiProvider } from '../providers/emoji';

@NgModule({
  declarations: [
    Oukazon,
    ClassifiedPage,
    DirectoryPage,
    AskPage,
    ProfilePage,
    SettingsPage,
    MessagesPage,
    HomePage,
    SmodalPage,
    AdDetailPage,
    CompDetailPage,
    CompcatPage,
    CompsubcatPage,
    AskDetailPage,
    AdcatPage,
    AdsubcatPage,
    PostAdPage,
    EditAdPage,
    PostCompPage,
    EditCompPage,
    PostAskPage,
    EditAskPage,
    MapAutocompletePage,
    LoginPage,
    ClangPage,
    SignupPage,
    TabsPage,
    WelcomePage,
    ParallaxDirective,
    TimeAgoPipe,
    PostyourPage
  ],
  imports: [
    BrowserModule, LinkyModule, MomentModule, MenuPageModule, ChatModule, IonicImageViewerModule, SelectSearchableModule, EmojiPickerComponentModule,
    IonicModule.forRoot(Oukazon),
    HttpModule, HttpClientModule, IonicStorageModule.forRoot(), IonicPageModule.forChild(Oukazon),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Oukazon,
    ClassifiedPage,
    DirectoryPage,
    AskPage,
    ProfilePage,
    SettingsPage,
    MessagesPage,
    HomePage,
    SmodalPage,
    AdDetailPage,
    AdcatPage,
    AdsubcatPage,
    CompDetailPage,
    CompcatPage,
    CompsubcatPage,
    AskDetailPage,
    PostAdPage,
    EditAdPage,
    PostCompPage,
    EditCompPage,
    PostAskPage,
    EditAskPage,
    MapAutocompletePage,
    LoginPage,
    ClangPage,
    SignupPage,
    TabsPage,
    WelcomePage,
    PostyourPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    ProductService,
    Common,
    GoogleMaps,
    Geolocation,
    NativeGeocoder,
    LocationAccuracy,
    File,
    Transfer,
    Camera,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MapProvider,
    SpinnerProvider,
    PayPal,
    EmojiProvider,
    LocalNotifications,
    SMS,
    CallNumber,
    InAppBrowser,
  ]
})
export class AppModule {
}
