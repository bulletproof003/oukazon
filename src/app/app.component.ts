import { Component, ViewChild } from '@angular/core';
import { App, Nav, AlertController, Platform, ToastController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { DirectoryPage } from '../pages/directory/directory';
import { ClassifiedPage } from '../pages/classified/classified';
import { AskPage } from '../pages/ask/ask';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { SettingsPage } from '../pages/settings/settings';
import { MessagesPage } from '../pages/messages/messages';
import { WelcomePage } from '../pages/welcome/welcome';
import { LoginPage } from '../pages/login/login';
import { AuthService } from '../providers/auth-service/auth-service';
import { Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

export interface PageInterface {
  title: string;
  pageName: any;
  component: any;
  tabComponent?: any;
  index?: number;
  badge?: number;
  icon: string;
}

@Component({
  templateUrl: 'app.html'
})
export class Oukazon {
  rootPage: any;
  allowClose: boolean;
  public lastBack=0;
  data: any;
  langdata: any;
  nb: any;
  nbmsgs: any;
  lang: any;
  nbmsgs1: any;
  userDetails : any;
  userPostData = {"id":"","token":""};
  responseData : any;
  refreshIntervalId2 : any;
  userLogged: boolean;
 
  // Reference to the app's root nav
  @ViewChild(Nav) nav: Nav;

  pages: PageInterface[] = [
    { title: 'Home', pageName: HomePage, component: HomePage, icon: 'home' },
    { title: 'Directory', pageName: DirectoryPage, component: DirectoryPage, icon: 'folder' },
    { title: 'Classified Ads', pageName: ClassifiedPage, component: ClassifiedPage, icon: 'cart' },
    { title: 'Ask OUKA', pageName: AskPage, component: AskPage, icon: 'help-circle' },
    { title: 'My Dashboard', pageName: ProfilePage, component: ProfilePage, icon: 'person' },
    { title: 'Account Settings', pageName: SettingsPage, component: SettingsPage, icon: 'settings' },
    { title: 'Messages', pageName: MessagesPage, 
     component: MessagesPage, 
     badge: 0, 
     icon: 'chatbubbles' },
  ];

  constructor(public toastCtrl: ToastController, statusBar: StatusBar, splashScreen: SplashScreen, private app:App, private alertCtrl: AlertController, public platform: Platform, private localNotifications: LocalNotifications, private events: Events, public authService:AuthService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleLightContent();
        splashScreen.hide();
        if (splashScreen) {
          setTimeout(() => {
              splashScreen.hide();
          }, 100);
      }
        platform.registerBackButtonAction(() => {
          const overlay = this.app._appRoot._overlayPortal.getActive();
          const nav = this.app.getActiveNavs()[0];
          const closeDelay = 2000;
          const spamDelay = 500;
        
          if(overlay && overlay.dismiss) {
            overlay.dismiss();
          } else if(nav.canGoBack()){
            nav.pop();
          } else if(Date.now() - this.lastBack > spamDelay && !this.allowClose) {
            this.allowClose = true;
            let toast = this.toastCtrl.create({
              message: "Press again to exit",
              duration: closeDelay,
              dismissOnPageChange: true,
              position: "top"
            });
            toast.onDidDismiss(() => {
              this.allowClose = false;
            });
            toast.present();
          } else if(Date.now() - this.lastBack < closeDelay && this.allowClose) {
            this.platform.exitApp();
          }
          this.lastBack = Date.now();
        });
    });
    
    this.load();
   }

   getcontent() {
    this.events.subscribe('username:changed', data => {
      if(this.userDetails === undefined || this.userDetails === ""){
        this.userDetails = data;
      }
   })
    
    this.check_Msgs();
    this.events.publish("nbmsgs", this.nbmsgs, 1);
    this.platform.ready().then((readySource) => {
      this.localNotifications.on('click', (notification, state) => {
        this.nav.push(MessagesPage);
      })
    });
    
   }

   load() {
     
    if (localStorage.getItem('userData')) {
      this.userLogged = true;
      this.data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = this.data.userData;
    this.userPostData.id = this.userDetails.id;
    this.userPostData.token = this.userDetails.token;
    console.log(this.data.userData);
      this.getcontent();
      setTimeout(() => {
        this.rootPage = HomePage;
      }, 700);
      
    } else {
      this.rootPage = WelcomePage;
      this.userLogged = false;
    }
    if (localStorage.getItem('clang')) {
      this.langdata = JSON.parse(localStorage.getItem('clang'));
      console.log("LANG "+this.langdata);
    }
   }

   setlangAR(){
    localStorage.removeItem('clang');
    this.lang = 'AR';
        localStorage.setItem('clang', JSON.stringify(this.lang));
        //this.app.getRootNav().setRoot(Oukazon);
        
        this.load();
        this.app.getRootNav().goToRoot();
    }

  setlangEN(){
    localStorage.removeItem('clang');
    this.lang = 'EN';
        localStorage.setItem('clang', JSON.stringify(this.lang));
        //this.app.getRootNav().setRoot(Oukazon);
        //window.location.reload();
        this.load();
        this.app.getRootNav().goToRoot();
    }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Language | الغة',
      message: 'أختر اللغة التى تريد ان يظهر بها المحتوى المنزل من قبل مستخدمي أوكازون <br> . Please Select the language in which you want the content posted by OUKAZON users to appear',
      buttons: [
        {
          text: 'English',
          handler: () => {
            this.setlangEN();
          }
        },
        {
          text: 'العربية',
          handler: () => {
            this.setlangAR();
          }
        }
      ]
    });
    alert.present();
  }

   presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to exit",
      duration: 3000,
      position: "top"
    });
    toast.present();
  }

   ngOnInit() {
    this.load();
    this.refreshIntervalId2 = setInterval(() => {
      this.check_Msgs();
      this.notify();
      this.events.publish("nbmsgs", this.nbmsgs, 1);
  }, 60000)
   }


  notify() {
    var b; var a;
    //that.currentLocation();
    b = parseFloat(this.nbmsgs1);
    a = parseFloat(this.nbmsgs);
    if (b==a && a=='0') {
      this.nbmsgs1 = this.nbmsgs;
    } else {
      if (this.nbmsgs1!=undefined && this.nbmsgs!=undefined) {
      this.localNotifications.schedule({
        id: 1,
        title: 'Oukazon Messenger',
        text: 'You have '+this.nbmsgs+' New Messages',
        icon: "res://icon.png",
        smallIcon:"res://icon.png"
      });
    }
      this.nbmsgs1 = this.nbmsgs;
    }
  }

   check_Msgs() {
    this.authService.postData(this.userPostData, "check_msg").then((result) => {
      this.responseData = result;
      if (this.responseData.check_msgData) {
        this.nb = this.responseData.check_msgData;
        this.nbmsgs = this.nb.nbmsg;
        this.events.publish("nbmsgs", this.nbmsgs, 1);
        console.log(this.nbmsgs);
      }
    })
  }

  continue() {
    let alert = this.alertCtrl.create({
      title: "DONE",
      message: 'Thank you for using OUKAZON App. Please Login To Continue !',
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Login",
          handler: () => {
            this.nav.push(LoginPage);
          }
        }
      ]
    });
    alert.present();
}

logout() {

    localStorage.removeItem('userData');
    this.nav.setRoot(Oukazon);
    clearInterval(this.refreshIntervalId2);
    this.continue();
    this.userDetails = "";


}

  openPage(page: PageInterface) {
    let params = {};
 
    // The index is equal to the order of our tabs inside tabs.ts
    if (page.index) {
      params = { tabIndex: page.index };
    }
 

      if (page.pageName == HomePage) {
        this.nav.setRoot(page.pageName, params);
      } else {
      // Tabs are not active, so reset the root page 
      // In this case: moving to or from SpecialPage
      this.nav.push(page.pageName);
      }
  }
 

  
}