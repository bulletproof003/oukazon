import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ProductService {
  private url: String;

  constructor(public http: Http) {
    this.url = "https://www.oukazon.com/api/api.php";
    console.log("Hi from ProductService");
  }

  getPayments() {
    return this.http.get(this.url + "?action=getconfig").map(resconfig => resconfig.json())
  }
  getCurrencies() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      this.http.get('http://apilayer.net/api/live?access_key=33f6db0fb866882326a71103273ca17e&currencies=USD,QAR&format=1', { headers: headers }).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
    });
  }

}