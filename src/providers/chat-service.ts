import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators/map';
import { HttpClient, } from "@angular/common/http";
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from "rxjs/Observable";

let apiUrl = 'https://www.oukazon.com/api/';
export class ChatMessage {
    messageId: string;
    userId: string;
    userName: string;
    uName: string;
    userAvatar: string;
    toUserId: string;
    toUserName: string;
    time: number | string;
    message: string;
    status: string;
    token: string;
}

@Injectable()
export class ChatService {

    constructor(public http : Http, 
                private httpclient: HttpClient) {
    }


    getMsgList(credentials, type): Observable<ChatMessage[]> {
        let headers = new Headers();
        return this.http.post(apiUrl + type, JSON.stringify(credentials), {headers: headers})
         .pipe(map(response => response.json()));
    }

}
