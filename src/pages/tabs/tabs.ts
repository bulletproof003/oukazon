import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

//import { DirectoryPage } from '../directory/directory';
//import { ClassifiedPage } from '../classified/classified';
//import { AskPage } from '../ask/ask';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  myIndex: number;

  constructor(navParams: NavParams) {
    // Set the active tab based on the passed index from menu.ts
    this.myIndex = navParams.data.tabIndex || 0;
  }
}