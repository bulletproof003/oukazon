import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AskDetailPage } from './ask-detail';

@NgModule({
  declarations: [
    AskDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(AskDetailPage),
  ],
})
export class AskDetailPageModule {}
