import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Common } from '../../providers/common';
import { WelcomePage } from '../welcome/welcome';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  public userDetails: any;
  resposeData: any;
  data: any;
  loading: Loading;

  editData = {
    id: "",
    token: "",
    address: "",
    city: "",
    country: "",
    description: "",
    email: "",
    facebook: "",
    googleplus: "",
    instagram: "",
    linkedin: "",
    name: "",
    phone: "",
    postcode: "",
    tagline: "",
    twitter: "",
    username: "",
  };

  passData = {
    id: "",
    token: "",
    ps: "",
    ps1: "",
  };

  constructor(private alertCtrl: AlertController, public common: Common, public loadingCtrl: LoadingController, public authService: AuthService, public navCtrl: NavController, public navParams: NavParams) {
    this.data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = this.data.userData;
    this.editData.id = this.userDetails.id;
    this.editData.token = this.userDetails.token;
    this.passData.id = this.userDetails.id;
    this.passData.token = this.userDetails.token;
    this.common.presentLoading();
    this.common.closeLoading();
  }

  Done() {
    let alert = this.alertCtrl.create({
      title: 'Congratulation',
      subTitle: 'Your Information Have been Updated !',
      buttons: ['Done']
    });
    alert.present();
  }

  Done1() {
    let alert = this.alertCtrl.create({
      title: 'Congratulation',
      subTitle: 'Your Password Have been Updated !, Please Login to continue',
      buttons: ['Done']
    });
    alert.present();
  }

  edit() {
    this.loading = this.loadingCtrl.create({
      content: 'Editing Data...',});
    this.loading.present();
    this.editData.address = this.userDetails.address;
    this.editData.city = this.userDetails.city;
    this.editData.country = this.userDetails.country;
    this.editData.description = this.userDetails.description;
    this.editData.email = this.userDetails.email;
    this.editData.facebook = this.userDetails.facebook;
    this.editData.googleplus = this.userDetails.googleplus;
    this.editData.instagram = this.userDetails.instagram;
    this.editData.linkedin = this.userDetails.linkedin;
    this.editData.name = this.userDetails.name;
    this.editData.postcode = this.userDetails.postcode;
    this.editData.phone = this.userDetails.phone;
    this.editData.tagline = this.userDetails.tagline;
    this.editData.twitter = this.userDetails.twitter;
    this.editData.username = this.userDetails.username;
    this.authService.postData(this.editData, "editUser").then((result) => {
      this.resposeData = result;
      if(this.resposeData.userData){
        localStorage.setItem('userData', JSON.stringify(this.resposeData) );
        this.loading.dismissAll()
          this.Done();
    }}, (err) => {
      alert('Problem Edit User Infos.');
    });
  }

  NoPass() {
    let alert = this.alertCtrl.create({
      title: 'Sorry',
      subTitle: 'Password must be between 4 and 20 characters long. !',
      buttons: ['OK']
    });
    alert.present();
  }

  NoPass1() {
    let alert = this.alertCtrl.create({
      title: 'Sorry',
      subTitle: 'Your Password is Wrong. !',
      buttons: ['OK']
    });
    alert.present();
  }

  NoEmpty() {
    let alert = this.alertCtrl.create({
      title: 'Problem , Password',
      subTitle: 'Please Fill Your Password !',
      buttons: ['OK']
    });
    alert.present();
  }

  NoEmpty1() {
    let alert = this.alertCtrl.create({
      title: 'Problem , New Password',
      subTitle: 'Please Fill Your New Password !',
      buttons: ['OK']
    });
    alert.present();
  }

  changePass(){
    var A; var B;
    A = this.passData.ps; B = this.passData.ps1; 
    if (A.length < 4 || A.length.length > 20) {
      this.NoPass();
    } else if (B.length < 4 || B.length > 20) {
      this.NoPass();
    } else if (this.passData.ps == '') {
      this.NoEmpty();
    } else if (this.passData.ps1 == '') {
      this.NoEmpty1();
    } else {
    this.loading = this.loadingCtrl.create({
    content: 'Processing...',});
    this.loading.present();
    this.authService.postData(this.passData, "editPass").then((result) => {
      this.resposeData = result;
      if (this.resposeData.success) {
        localStorage.removeItem('userData');
        this.navCtrl.setRoot(WelcomePage);
        this.loading.dismissAll()
          this.Done1();
        } else {
          this.loading.dismissAll()
          this.NoPass1();
        }
    }, (error) => {
      alert(error);
    });
  }
  }

  




}
