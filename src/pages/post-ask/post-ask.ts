import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ViewController, ModalController, ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { MapProvider } from '../../providers/map/map';
import { AuthService } from '../../providers/auth-service/auth-service';
import { SelectSearchable } from '../../components/select/select';

declare var cordova: any;
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-post-ask',
  templateUrl: 'post-ask.html',
})
export class PostAskPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('searchbar', { read: ElementRef }) searchbar: ElementRef;
  addressElement: HTMLInputElement = null;
  responseData: any;
  cityData: any;
  cityList: any;
  askData = {
    token: "",
    user_id: "",
    question_name: "",
    question_namear: "",
    description: "",
    descriptionar: "",
    phone: "",
    hide_phone: "",
    location: "",
    city: "",
    state: "",
    country: "178",
    latlong: "",
    screen_shot: "",
    tag: "",
    status: "active",
    contact_phone: "",
    contact_email: "",
    contact_chat: "",
  };
  locationData = {
    cc: ""
  };
  locationData1: any;
  cityDetails = { "userid": "", "token": "" };
  hide_phone = { value: false };
  contact_phone = { value: true };
  contact_email = { value: true };
  contact_chat = { value: true };
  agree = { value: true };
  terms = {
    agrees: ""
  };
  map: any;
  address = '';
  latlongs: any;
  public photos: any;
  public base64Image: string;
  public image: any;
  infoWindow: any;
  askLocation: String;
  askLoc: any;
  lastImage: string = null;
  loading: Loading;
  public userDetails: any;
  constructor(private alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public zone: NgZone,
    public localStore: Storage,
    public mapService: MapProvider,
    public modalCtrl: ModalController,
    public toaster: ToastController,
    public locac: LocationAccuracy,
    public geocoder: NativeGeocoder,
    public geolocation: Geolocation,
    public navParams: NavParams,
    private transfer: Transfer,
    private file: File,
    private filePath: FilePath,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public platform: Platform,
    public authService: AuthService,
    public navCtrl: NavController,
    private camera: Camera,
    public loadingCtrl: LoadingController) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data.userData;
    this.askData.user_id = this.userDetails.id;
    this.askData.token = this.userDetails.token;
    this.cityDetails.userid = this.userDetails.id;
    this.cityDetails.token = this.userDetails.token;
    this.h_phone();
    this.phone();
    this.email();
    this.chat();
    this.agreeTerms();
    this.getCities();
  }
  ionViewDidLoad() { }
  getCities() {
    this.authService.postData(this.cityDetails, "getCity").then((result) => {
      this.cityData = result;
      if (this.cityData.getCityList) {
        this.cityList = this.cityData.getCityList;
        console.log(this.cityList);
      }
      else {
        console.log("problem cities");
      }
    }, (err) => {

    });
  }
  portChange(event: { component: SelectSearchable, value: any }) {
    console.log('value:', event.value);
  }

  h_phone() {
    if (this.hide_phone.value == false) { this.askData.hide_phone = "0"; } else { this.askData.hide_phone = "1"; }
  }
  phone() {
    if (this.contact_phone.value == true) { this.askData.contact_phone = "1"; } else { this.askData.contact_phone = "0"; }
  }
  email() {
    if (this.contact_email.value == true) { this.askData.contact_email = "1"; } else { this.askData.contact_email = "0"; }
  }
  chat() {
    if (this.contact_chat.value == true) { this.askData.contact_chat = "1"; } else { this.askData.contact_chat = "0"; }
  }
  agreeTerms() {
    if (this.agree.value == true) { this.terms.agrees = "1"; } else { this.terms.agrees = "0"; }
  }

  ngOnInit() {
    //this.platform.ready().then(() => this.loadMaps());
    this.requestAccuracy();
    this.photos = [];
  }
  
  requestAccuracy() {
    this.locac.canRequest().then((canRequest: boolean) => {
      if(canRequest) {
        this.locac.request(this.locac.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
            console.log('Request successful.');
            this.platform.ready().then(() => this.loadMaps());
          },
          error => {
            console.log('Error requesting location permissions', error)
            this.closeModal();
          }
        );
      }
    });
  }

  loadMaps() {
    if (!!google) {
      this.initializeMap();
      this.initAutocomplete();
    } else {
      this.errorAlert('Error', 'Something went wrong with the Internet Connection. Please check your Internet.')
    }
  }

  initializeMap() {
    let that = this;
    that.currentLocation();
    this.zone.run(() => {
      var mapEle = this.mapElement.nativeElement;
      this.map = new google.maps.Map(mapEle, {
        zoom: 14,
        center: { lat: 25.2854473, lng: 51.53104 },
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        //styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }],
        disableDoubleClickZoom: false,
        disableDefaultUI: true,
        zoomControl: true,
        scaleControl: true,
        componentRestrictions: {
          country: 'QA',
        }
      });


      // Map drag started
      this.map.addListener('dragstart', function () {
        console.log('Drag start');
      });
      // Map dragging
      this.map.addListener('drag', function () {
        that.address = 'Searching...';
      });
      //Reload markers every time the map moves
      this.map.addListener('dragend', function () {
        let map_center = that.getMapCenter();
        let latLngObj = { 'lat': map_center.lat(), 'long': map_center.lng() };
        console.log(latLngObj);
        that.getAddress(latLngObj);
      });

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        google.maps.event.trigger(this.map, 'resize');
        mapEle.classList.add('show-map');
      });

      google.maps.event.addListener(this.map, 'bounds_changed', () => {
        this.zone.run(() => {
          this.resizeMap();
        });
      });


    });
  }

  initAutocomplete(): void {
    this.addressElement = this.searchbar.nativeElement.querySelector('.searchbar-input');
    this.createAutocomplete(this.addressElement).subscribe((location) => {
      console.log('Searchdata', location);
      let latLngObj = { 'lat': location.lat(), 'long': location.lng() };
      this.askData.latlong = location.lat() + "," + location.lng();
      console.log(this.askData.latlong);
      this.getAddress(latLngObj);
      console.log(latLngObj);
      console.log("22222222222");
      let options = {
        center: location,
        zoom: 14,
        componentRestrictions: { country: 'QA' }
      };
      this.map.setOptions(options);
    });
  }

  currentLocation() {
    this.geolocation.getCurrentPosition().then((position) => {
      //let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let latLngObj = { 'lat': position.coords.latitude, 'long': position.coords.longitude };
      this.latlongs = position.coords.latitude + "," + position.coords.longitude;
      console.log(this.latlongs);
      this.askData.latlong = this.latlongs;
      // Display  Marker
      this.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
      this.getAddress(latLngObj);

      localStorage.setItem('current_latlong', JSON.stringify(latLngObj));
      console.log(latLngObj);
      console.log("3333333333");
      return latLngObj;


    }, (err) => {
      console.log(err);
    });
  }

  getAddress(latLngObj) {
    // Get the address object based on latLngObj
    this.mapService.getStreetAddress(latLngObj).subscribe(
      s_address => {
        if (s_address.status == "ZERO_RESULTS") {
          this.mapService.getAddress(latLngObj).subscribe(
            address => {
              this.address = address.results[0].formatted_address;
              this.askData.location = this.address;
              this.getAddressComponentByPlace(address.results[0], latLngObj);
            },
            err => console.log("Error in getting the street address " + err)
          )
        } else {
          this.address = s_address.results[0].formatted_address;
          this.askData.location = this.address;
          this.getAddressComponentByPlace(s_address.results[0], latLngObj);
          console.log(this.address);
        }
      },
      err => {
        console.log('No Address found ' + err);
      }
    );

  }

  getMapCenter() {
    return this.map.getCenter()
  }

  createAutocomplete(addressEl: HTMLInputElement): Observable<any> {
    const autocomplete = new google.maps.places.Autocomplete(addressEl);
    autocomplete.bindTo('bounds', this.map);
    return new Observable((sub: any) => {
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        const place = autocomplete.getPlace();
        if (!place.geometry) {
          sub.error({
            message: 'Autocomplete returned place with no geometry'
          });
        } else {
          let latLngObj = { 'lat': place.geometry.location.lat(), 'long': place.geometry.location.lng() }
          this.getAddress(latLngObj);
          sub.next(place.geometry.location);
        }
      });
    });
  }

  getAddressComponentByPlace(place, latLngObj) {
    var components;

    components = {};

    for (var i = 0; i < place.address_components.length; i++) {
      let ac = place.address_components[i];
      components[ac.types[0]] = ac.long_name;
    }
    let addressObj = {

      street: (components.street_number) ? components.street_number : 'not found',
      area: components.route,
      city: (components.sublocality_level_1) ? components.sublocality_level_1 : components.locality,
      country: (components.administrative_area_level_1) ? components.administrative_area_level_1 : components.political,
      postCode: components.postal_code,
      loc: [latLngObj.long, latLngObj.lat],
      address: this.address
    }
    localStorage.setItem('carryr_customer', JSON.stringify(addressObj));
    return components;
  }

  resizeMap() {
    setTimeout(() => {
      google.maps.event.trigger(this.map, 'resize');
    }, 200);
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  errorAlert(title, message) {
    alert('Error in Alert');
  }

  makeid() {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  };

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 60,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      targetWidth: 1000,
      targetHeight: 1000,
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      if (this.photos.length < 5) {
        this.photos.push(this.lastImage);
      } else {
        this.presentToast('Maximum 5 Pictures allowed.');
      }
      this.photos.reverse();
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(imgs) {
    if (imgs === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + imgs;
    }
  }

  uploadImage() {
    // Destination URL
    var url = "https://www.oukazon.com/app_upload/ask_uploads.php";
    const fileTransfer: TransferObject = this.transfer.create();
    var i;

    for (i = 0; i < this.photos.length; i++) {
      // File for Upload
      var targetPath = this.pathForImage(this.photos[i]);

      // File name only
      var filename = this.photos[i];
      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params: { 'fileName': filename }
      };

      // Use the FileTransfer to upload the image
      fileTransfer.upload(targetPath, url, options).then(data => {
      }, err => {
        this.presentToast('Error while uploading file.');
      });
    }
    this.presentToast('All Images succesfully uploaded.');
  }

  deletePhoto(index) {
    let confirm = this.alertCtrl.create({
      title: 'Sure you want to delete this photo? There is NO undo!',
      message: '',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Agree clicked');
            this.photos.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  titleAlert() {
    let alert = this.alertCtrl.create({
      title: 'Title Field',
      subTitle: 'Please, Give Your Question a title, (at least one Language required) !',
      buttons: ['OK']
    });
    alert.present();
  }

  descAlert() {
    let alert = this.alertCtrl.create({
      title: 'Description Field',
      subTitle: 'Please, Give Your Question a Description, (at least one Language required) !',
      buttons: ['OK']
    });
    alert.present();
  }

  cityAlert() {
    let alert = this.alertCtrl.create({
      title: 'CITY Field',
      subTitle: 'CITY Field is required ! Please select a City',
      buttons: ['OK']
    });
    alert.present();
  }

  agreeAlert() {
    let alert = this.alertCtrl.create({
      title: 'Agree to Terms',
      subTitle: 'Please, Check the AGREE checkBox If You agree to the terms and conditions. !',
      buttons: ['OK']
    });
    alert.present();
  }

  Done() {
    let alert = this.alertCtrl.create({
      title: 'Congratulation',
      subTitle: 'Your Question Have been Posted !',
      buttons: ['Done']
    });

    alert.present();
  }

  postAsk() {
    this.locationData1 = this.locationData.cc;
    this.askData.city = this.locationData1.city_id;
    this.askData.state = this.locationData1.state_id;
    if (this.askData.question_namear === '' && this.askData.question_name === '') {
      this.titleAlert();
    } else if (this.askData.descriptionar === '' && this.askData.description === '') {
      this.descAlert();
    } else if (this.askData.city === undefined) {
      this.cityAlert();
    } else if (this.terms.agrees === '0') {
      this.agreeAlert();
    } else {
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: 'Loading Please Wait...'
      }); loading.present();
      if (this.photos.length > 0) {
        this.uploadImage();
      } else {
        this.askData.screen_shot = '';
      }
      this.authService.postData(this.askData, "postAsk").then((result) => {
        this.presentToast('Succesfully Posted.');
        setTimeout(() => {}, 1000);
      }, (err) => {
        this.presentToast('Problem While Posting Data.');
      });
      setTimeout(() => {
        loading.dismiss();
        this.navCtrl.pop();
        this.Done();
      }, 5000);

    }

  }



}