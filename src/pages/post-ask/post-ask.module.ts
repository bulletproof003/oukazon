import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostAskPage } from './post-ask';

@NgModule({
  declarations: [
    PostAskPage,
  ],
  imports: [
    IonicPageModule.forChild(PostAskPage),
  ],
})
export class PostAskPageModule {}
