import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Chat } from '../chat/chat';
import { Common } from '../../providers/common';

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {
  userPostData = {"id":"","token":""};
  userPostData1 = {"id":"","token":""};
  userDetails : any;
  responseData : any;
  responseData1 : any;
  msgs : any;
  msgs1 : any;
  msgs2 : any;
  constructor(public common: Common, public navCtrl: NavController, public navParams: NavParams, public authService:AuthService) {
    
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
    this.userPostData.id = this.userDetails.id;
    this.userPostData.token = this.userDetails.token;
    this.userPostData1.id = this.userDetails.id;
    this.userPostData1.token = this.userDetails.token;
   
  }

  ionViewWillEnter() {
    this.common.presentLoading();
    this.getContactList();
    this.getContactList1();
    setTimeout(() => {
      this.totalmsg();
  }, 3000)
    this.common.closeLoading();
  }

  getContactList() {
    this.authService.postData(this.userPostData, "contactList").then((result) => {
      this.responseData = result;
      if (this.responseData.contactListData1) {
        this.msgs1 = this.responseData.contactListData1;
        //for (let i = 0; i < this.msgs.length; i++) {

        //}
        console.log(this.msgs1);
        console.log(this.userPostData1);
      }
    })
  }

  getContactList1() {
    this.authService.postData(this.userPostData1, "contactList1").then((result) => {
      this.responseData1 = result;
      if (this.responseData1.contactListData2) {
        this.msgs2 = this.responseData1.contactListData2;
      }
    })
  }

  totalmsg() {
    if (this.msgs2.length>0) {
      for (let i = 0; i < this.msgs1.length; i++) {
        for (let m = 0; m < this.msgs2.length; m++) {
          if (this.msgs2[m].from_id == this.msgs1[i].id) {
            this.msgs1[i].seen = this.msgs2[m].NB;
          } else {
            this.msgs1[i].seen = '0';
          }
        }

      }
      console.log(this.msgs1);
    }
  }


  openchat(msgIndex) {this.navCtrl.push(Chat, {"toUser":this.msgs1[msgIndex]});}

  getMsg() {
    this.authService.postData(this.userPostData1, "messages").then((result) => {
      this.responseData1 = result;
      if (this.responseData1.messagesData) {
        this.msgs = this.responseData1.messagesData;
        for (let i = 0; i < this.msgs.length; i++) {
          if (this.msgs[i].userId == this.userDetails.id) {
          this.msgs[i].userName = this.userDetails.name;
          this.msgs[i].userImgUrl = "https://www.oukazon.com/storage/profile/small_"+this.userDetails.image;
          this.msgs[i].userAvatar = "https://www.oukazon.com/storage/profile/small_"+this.msgs[i].userAvatar;
          } else{
          this.msgs[i].toUserName = this.userDetails.name;
          this.msgs[i].userImgUrl = "https://www.oukazon.com/storage/profile/small_"+this.msgs[i].userAvatar;
          this.msgs[i].userAvatar = "https://www.oukazon.com/storage/profile/small_"+this.userDetails.image;
          }
        }
        console.log(this.msgs);
      }
    })
  }




}
