import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { PostCompPage } from '../post-comp/post-comp';

@Component({
  selector: 'page-compsubcat',
  templateUrl: 'compsubcat.html',
})
export class CompsubcatPage {
  subcat: any;
  subcatDetails = { "userid": "", "token": "", "cat_id": "" };
  subcatData: any;
  subcatList: any;
  userDetails: any;
  langData: any;
  constructor(public authService: AuthService, public navCtrl: NavController, public navParams: NavParams) {
    this.langData = JSON.parse(localStorage.getItem('clang'));
    this.subcat = this.navParams.get("cat");
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data.userData;
    this.subcatDetails.userid = this.userDetails.id;
    this.subcatDetails.token = this.userDetails.token;
    this.subcatDetails.cat_id = this.subcat.cat_id;
    this.getsubCat();
  }

  getsubCat() {
    this.authService.postData(this.subcatDetails, "companysubcat").then((result) => {
      this.subcatData = result;
      if (this.subcatData.getCompSubCatList) {
        this.subcatList = this.subcatData.getCompSubCatList;
        console.log(this.subcatList);
      }
      else {
        console.log("problem sub_categories");
      }
    }, (err) => {

    });
  }

  postcompSubCat(i){
    this.navCtrl.push(PostCompPage, {"adsubcat":this.subcatList[i]})
    //.then(() => {
    //  let index = this.navCtrl.length()-2;
     // this.navCtrl.remove(index);
    //});
  }


}
