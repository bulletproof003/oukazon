import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { Events, Content, TextInput } from 'ionic-angular';
import { ChatService, ChatMessage } from "../../providers/chat-service";
import { Common } from '../../providers/common';
import { AuthService } from '../../providers/auth-service/auth-service';

@IonicPage()
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class Chat {

    @ViewChild(Content) content: Content;
    @ViewChild('chat_input') messageInput: TextInput;
    msgList: ChatMessage[] = [];
    user: any;
    toUser: any;
    editorMsg = '';
    showEmojiPicker = false;
    userPostData = {"id":"","toUser":"","token":""};
    userPostData1 = {"id":"","toUser":"","token":""};
    userDetails: any;
    responseData: any;
    customData: any;
    refreshIntervalId: any;

    constructor(navParams: NavParams,
                public common: Common,
                private chatService: ChatService,
                private events: Events,
                public authService:AuthService) {
        this.common.presentLoading();
        const data = JSON.parse(localStorage.getItem('userData'));
        this.userDetails = data.userData;
        this.userPostData.id = this.userDetails.id;
        this.userPostData.token = this.userDetails.token;
        this.userPostData1.id = this.userDetails.id;
        this.userPostData1.token = this.userDetails.token;
        const Data = navParams.get("toUser");
        var id; var name; var username;
            // Get the navParams toUserId parameter
        id = Data.id;
        if (Data.name == "") {
            name = this.userDetails.username;
        } else {
            name = Data.name;
        }
        
        username = Data.username;
        this.toUser = {
            id: id,
            name: name,
            uname: username
        };
        this.userPostData.toUser = this.toUser.id;
        this.userPostData1.toUser = this.toUser.id;
        // Get mock user information
        this.getUserInfo()
        .then((res) => {
            this.user = res
        });
        this.refreshIntervalId = setInterval(() => {
            this.getMsg();
        }, 3000)
        this.common.closeLoading();
    }

    getUserInfo(){
        const userInfo: any = {
            id: this.userDetails.id,
            name: this.userDetails.name,
            avatar: "https://www.oukazon.com/storage/profile/small_"+this.userDetails.image
        };
        return new Promise(resolve => resolve(userInfo));
    }

    ionViewWillLeave() {
        // unsubscribe
        this.events.unsubscribe('chat:received');
        clearInterval(this.refreshIntervalId);
    }

    ionViewDidEnter() {
        //get message list
        // Subscribe to received  new message events
        this.events.subscribe('chat:received', msg => {
            this.pushNewMsg(msg);
        })
        this.setseen();

    }

    setseen() {
        this.authService.postData(this.userPostData1, "setSeen").then((result) => {
          this.responseData = result;

        })
      }

    

    onFocus() {
        this.showEmojiPicker = false;
        this.content.resize();
        this.scrollToBottom();
    }

    switchEmojiPicker() {
        this.showEmojiPicker = !this.showEmojiPicker;
        if (!this.showEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.content.resize();
        this.scrollToBottom();
    }

    /**
     * @name getMsg
     * @returns {Promise<ChatMessage[]>}
     */
    private getMsg() {
        // Get mock message list
        return this.chatService
        .getMsgList(this.userPostData, "messages")
        .subscribe(res => {
            this.customData = res;
            if (this.customData.messagesData) {
                this.msgList = this.customData.messagesData.reverse();
                for (let i = 0; i < this.msgList.length; i++) {
                  if (this.msgList[i].userId == this.userDetails.id) {
                  this.msgList[i].userName = this.userDetails.name;
                  this.msgList[i].userAvatar = "https://www.oukazon.com/storage/profile/small_"+this.userDetails.image;
                  } else{
                    this.msgList[i].userName = this.toUser.name;
                  this.msgList[i].userAvatar = "https://www.oukazon.com/storage/profile/small_"+this.msgList[i].userAvatar;
                  }
                }

                console.log(this.msgList);
              }

        });
        
    }

    /**
     * @name sendMsg
     */
    sendMsg() {
        if (!this.editorMsg.trim()) return;

        // Mock message
        const id = Date.now().toString();
        var zzz = Date.now();
        var date = new Date(zzz);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        var ids = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
        let newMsg: ChatMessage = {
            messageId: Date.now().toString(),
            userId: this.user.id,
            userName: this.user.name,
            uName: this.userDetails.username,
            userAvatar: this.user.avatar,
            toUserId: this.toUser.id,
            toUserName: this.toUser.uname,
            time: ids,
            message: this.editorMsg,
            status: 'pending',
            token: this.userDetails.token,
        };

        console.log(newMsg);

        this.pushNewMsg(newMsg);
        this.editorMsg = '';

        if (!this.showEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.authService.postData(newMsg, "sendMessages")
            let index = this.getMsgIndexById(id);
            if (index !== -1) {
                this.msgList[index].status = 'success';
            }
    }

    /**
     * @name pushNewMsg
     * @param msg
     */
    pushNewMsg(msg: ChatMessage) {
        const userId = this.user.id,
              toUserId = this.toUser.id;
        // Verify user relationships
        if (msg.userId === userId && msg.toUserId === toUserId) {
            this.msgList.push(msg);
        } else if (msg.toUserId === userId && msg.userId === toUserId) {
            this.msgList.push(msg);
        }
        console.log(this.msgList);
        this.scrollToBottom();
    }

    getMsgIndexById(id: string) {
        return this.msgList.findIndex(e => e.messageId === id)
    }

    scrollToBottom() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom();
            }
        }, 400)
    }
}
