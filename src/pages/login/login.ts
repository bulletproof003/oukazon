import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams, ToastController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { SignupPage } from '../signup/signup';
import { Oukazon } from '../../app/app.component';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  responseData : any;
  data : any;
  userData = {"username":"", "password_hash":""};

  constructor(public events: Events, private app:App, public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, private toastCtrl:ToastController) {
    
  }

  login(){
    if(this.userData.username && this.userData.password_hash){
      this.authService.postData(this.userData,"login").then((result) => {
      this.responseData = result;
      if(this.responseData.userData){
        localStorage.setItem('userData', JSON.stringify(this.responseData) );
        const data = this.responseData.userData;
        this.events.publish('username:changed', data);
          this.navCtrl.setRoot(Oukazon);
      }
    else {
    this.presentToast("USERNAME OR PASSWORD is Wrong");
    }
  }, (err) => {
    // Connection failed message
  });
   }
   else{
    this.presentToast("Please Provide a Valid Username & Password");
   }
}

presentToast(msg) {
  let toast = this.toastCtrl.create({
    message: msg,
    duration: 3000,
    position: 'top'
  });
  toast.present();
}

sup(){
  this.navCtrl.push(SignupPage);
  }
}
