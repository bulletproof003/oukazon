import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { PostAdPage } from '../post-ad/post-ad';

@Component({
  selector: 'page-adsubcat',
  templateUrl: 'adsubcat.html',
})
export class AdsubcatPage {
  subcat: any;
  subcatDetails = { "userid": "", "token": "", "cat_id": "" };
  subcatData: any;
  subcatList: any;
  subcatListL: any;
  langData: any;
  userDetails: any;

  constructor(public authService: AuthService, public navCtrl: NavController, public navParams: NavParams) {
    this.langData = JSON.parse(localStorage.getItem('clang'));
    this.subcat = this.navParams.get("cat");
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data.userData;
    this.subcatDetails.userid = this.userDetails.id;
    this.subcatDetails.token = this.userDetails.token;
    this.subcatDetails.cat_id = this.subcat.cat_id;
    this.getsubCat();
  }

  getsubCat() {
    this.authService.postData(this.subcatDetails, "productsubcat").then((result) => {
      this.subcatData = result;
      if (this.subcatData.getAdSubCatList) {
        this.subcatList = this.subcatData.getAdSubCatList;
        this.subcatListL = this.subcatList.length;
        console.log(this.subcatList);
      }
      else {
        console.log("problem sub_categories");
      }
    }, (err) => {

    });
  }

  postadSubCat(i){
    this.navCtrl.push(PostAdPage, {"adsubcat":this.subcatList[i]});
    //.then(() => {
    //  let index = this.navCtrl.length()-2;
    //  this.navCtrl.remove(index);
    //});
  }


}
