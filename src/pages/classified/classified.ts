import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Common } from '../../providers/common';
import { AdDetailPage } from '../ad-detail/ad-detail';
import { DirectoryPage } from '../directory/directory';
import { AskPage } from '../ask/ask';

@Component({
  selector: 'page-classified',
  templateUrl: 'classified.html'
})
export class ClassifiedPage {
  catDetails = { "userid": "", "token": "" };
  catData: any;
  catList: any;
  public userDetails: any;

  subcat: any;
  subcatDetails = { "userid": "", "token": "", "cat_id": "" };
  subcatData: any;
  subcatList: any;
  title: any;

  step1: boolean;
  step2: boolean;
  step3: boolean;

  products: any;
  productsData: any;
  noRecords: boolean;
  responseData: any;
  langData: any;
  userPostData = { 
    id: "", 
    token: "",
    updated_at: "",
    cat_id: "",
    sub_cat_id: "",
  };
  constructor(public authService: AuthService, public common: Common, public navCtrl: NavController, public navParams: NavParams) {
    this.langData = JSON.parse(localStorage.getItem('clang'));
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data.userData;
    this.catDetails.userid = this.userDetails.id;
    this.catDetails.token = this.userDetails.token;
    this.subcatDetails.userid = this.userDetails.id;
    this.subcatDetails.token = this.userDetails.token;
  }

  ionViewWillEnter() {
    this.common.presentLoading();
    this.step1 = true;
    this.step2 = false;
    this.step3 = false;
    this.userPostData.updated_at = '';
    this.noRecords = false;
    this.getCat();
    this.catData = [];
    this.subcatData = [];
    this.subcatList = [];
    this.responseData = [];
    this.productsData = [];
    this.products = [];
    this.common.closeLoading();
  }

  showAd(i) {
    this.navCtrl.push(AdDetailPage, { "product": this.products[i] });
  }

  pushC() {
    this.navCtrl.push(DirectoryPage);
  }

  pushA() {
    this.navCtrl.push(AskPage);
  }

  getCat() {
    this.authService.postData(this.catDetails, "productcat").then((result) => {
      this.catData = result;
      if (this.catData.getAdCatList) {
        this.catList = this.catData.getAdCatList;
        console.log(this.catList);
      }
      else {
        console.log("problem categories");
      }
    }, (err) => {

    });
  }

  getsubCat() {
    
    this.authService.postData(this.subcatDetails, "productsubcat").then((result) => {
      this.subcatData = result;
      if (this.subcatData.getAdSubCatList) {
        this.subcatList = this.subcatData.getAdSubCatList;
        console.log(this.subcatList);
      }
      else {
        console.log("problem sub_categories");
      }
    }, (err) => {

    });
  }

  adSubCat(i){
    this.common.presentLoading();
    this.step1 = false;
    this.step2 = true;
    this.subcat = this.catList[i];
    console.log(this.subcat);
    console.log(this.catList[i]);
    this.subcatDetails.cat_id = this.subcat.cat_id;
    this.getsubCat();
    this.common.closeLoading();
  }

  postadSubCat(i){
    this.userPostData.id = this.userDetails.id;
    this.userPostData.token = this.userDetails.token;
    this.userPostData.cat_id = this.subcatList[i].main_cat_id;
    this.userPostData.sub_cat_id = this.subcatList[i].sub_cat_id;
    this.title = this.subcatList[i].sub_cat_name;
    console.log(this.subcatList[i]);
    this.step2 = false;
    this.step3 = true;
    this.getProduct();

  }

  backToCat(){
    this.common.presentLoading();
    this.subcat = [];
    this.subcatList = [];
    this.step1 = true;
    this.step2 = false;
    this.step3 = false;
    this.common.closeLoading();
  }

  backToSubCat(){
    this.common.presentLoading();
    this.getsubCat();
    this.userPostData.updated_at = '';
    this.noRecords = false;
    this.products = [];
    this.productsData = [];
    this.step2 = true;
    this.step3 = false;
    this.common.closeLoading();
  }

  doRefresh(refresher) {
    this.userPostData.updated_at = '';
    this.noRecords = false;
    this.products = [];
    this.productsData = [];

    this.getProduct();


    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }


  getProduct() {
    this.common.presentLoading();
    this.authService.postData(this.userPostData, "products1").then((result) => {
      this.responseData = result;
      if (this.responseData.productData0) {
        this.products = this.responseData.productData0;
        this.userPostData.updated_at = this.products[this.products.length - 1].updated_at;
        console.log("Updated At " + this.userPostData.updated_at);
        this.products.custom = '';
        this.products.checkbox_value = '';
        var i; var l;
        for (i = 0; i < this.products.length; i++) {
          this.products[i].userpic = "https://www.oukazon.com/storage/profile/small_" + this.products[i].image;
          this.products[i].screen_shot = this.products[i].screen_shot.split(",");
          this.products[i].tag = this.products[i].tag.split(",");
          this.products[i].latlong = this.products[i].latlong.split(",");
          this.products[i].lat = this.products[i].latlong[0];
          this.products[i].long = this.products[i].latlong[1];
          this.products[i].pictures = this.products[i].screen_shot;
          this.products[i].screen_shot = "https://www.oukazon.com/storage/products/screenshot/small_" + this.products[i].screen_shot[0];
          this.products[i].custom_types = this.products[i].custom_types.split(",");
          this.products[i].custom_values = this.products[i].custom_values.split(",");
          this.products[i].custom_fields = this.products[i].custom_fields.split(",");
          for (l = 0; l < this.products[i].custom_types.length; l++) {
            if (this.products[i].custom_types[l] !== "checkbox") {
              this.products[i].custom_types[l] = { type: this.products[i].custom_types[l], title: this.products[i].custom_fields[l], value: this.products[i].custom_values[l] };
            } else {
              if (this.products[i].custom_values[l] !== '') { this.products[i].checkbox_title = this.products[i].custom_fields[l]; }
              this.products[i].checkbox_value = this.products[i].custom_values[l];
            }
          }
          if (this.products[i].checkbox_value !== undefined) { this.products[i].checkbox_value = this.products[i].checkbox_value.split("+"); }
          else { this.products[i].checkbox_value = ''; }
          this.products[i].custom = this.products[i].custom_types;
        }
        console.log(this.products);
      } else {
        console.log("no");
      }
    }, (err) => {

    });
    this.common.closeLoading();
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      
      this.authService.postData(this.userPostData, "products1").then((result) => {
        this.responseData = result;
        if (this.responseData.productData0.length) {
          this.productsData = this.responseData.productData0;
          this.userPostData.updated_at = this.productsData[this.productsData.length - 1].updated_at;
          var a; var b;
          
          this.productsData.custom = '';
          this.productsData.checkbox_value = '';
          for (a = 0; a < this.productsData.length; a++) {
            this.productsData[a].userpic = "https://www.oukazon.com/storage/profile/small_" + this.productsData[a].image;
            this.productsData[a].screen_shot = this.productsData[a].screen_shot.split(",");
            this.productsData[a].tag = this.productsData[a].tag.split(",");
            this.productsData[a].latlong = this.productsData[a].latlong.split(",");
            this.productsData[a].lat = this.productsData[a].latlong[0];
            this.productsData[a].long = this.productsData[a].latlong[1];
            this.productsData[a].pictures = this.productsData[a].screen_shot;
            this.productsData[a].screen_shot = "https://www.oukazon.com/storage/products/screenshot/small_" + this.productsData[a].screen_shot[0];
            this.productsData[a].custom_types = this.productsData[a].custom_types.split(",");
            this.productsData[a].custom_values = this.productsData[a].custom_values.split(",");
            this.productsData[a].custom_fields = this.productsData[a].custom_fields.split(",");
            for (b = 0; b < this.productsData[a].custom_types.length; b++) {
              if (this.productsData[a].custom_types[b] !== "checkbox") {
                this.productsData[a].custom_types[b] = { type: this.productsData[a].custom_types[b], title: this.productsData[a].custom_fields[b], value: this.productsData[a].custom_values[b] };
              } else {
                if (this.productsData[a].custom_values[b] !== '') { this.productsData[a].checkbox_title = this.productsData[a].custom_fields[b]; }
                this.productsData[a].checkbox_value = this.productsData[a].custom_values[b];
              }
            }
            if (this.productsData[a].checkbox_value !== undefined) { this.productsData[a].checkbox_value = this.productsData[a].checkbox_value.split("+"); }
            else { this.productsData[a].checkbox_value = ''; }
            this.productsData[a].custom = this.productsData[a].custom_types;
          }

          for (let m = 0; m < this.productsData.length; m++) {
            this.products.push(this.productsData[m]);
          }
          console.log(this.products);
        } else {
          this.noRecords = true;
        }
      }, (err) => {
  
      });
      
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }









}
