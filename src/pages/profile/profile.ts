import { Component } from '@angular/core';
import { NavController, App, AlertController  } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Common } from '../../providers/common';
import { AdDetailPage } from '../ad-detail/ad-detail';
import { EditAdPage } from '../edit-ad/edit-ad';
import { EditCompPage } from '../edit-comp/edit-comp';
import { EditAskPage } from '../edit-ask/edit-ask';
import { CompDetailPage } from '../comp-detail/comp-detail';
import { AskDetailPage } from '../ask-detail/ask-detail';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
    my: string = "ads";
    public userDetails : any;
    public responseData: any;
    public resposeData: any;
    public dataSet : any;
    public dataSetRe : any;
    public dataSetFav : any;
    public dataSetHide : any;
    public dataSetPending : any;
    public dataSetComp : any;
    public dataSetCompRe : any;
    public dataSetCompHide : any;
    public dataSetCompPending : any;
    public dataSetAsk : any;
    public dataSetAskRe : any;
    public dataSetAskHide : any;
    public dataSetAskPending : any;
    showAd : boolean = false;
    showAd1 : boolean = false;
    showAd2 : boolean = false;
    showAd3 : boolean = false;
    showAd4 : boolean = false;
    showComp : boolean = false;
    showComp2 : boolean = false;
    showComp3 : boolean = false;
    showComp4 : boolean = false;
    showAsk : boolean = false;
    showAsk2 : boolean = false;
    showAsk3 : boolean = false;
    showAsk4 : boolean = false;
    langData : any;
    
    userPostData = {"id":"","token":""};

    userDeleteData = {
      user_id: "",
      token: "",
      id: "",
    };
  
    constructor(public common: Common, public navCtrl: NavController, public authService:AuthService, public app: App, private alertCtrl: AlertController) {
    this.langData = JSON.parse(localStorage.getItem('clang'));
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
    this.userPostData.id = this.userDetails.id;
    this.userPostData.token = this.userDetails.token;
    this.userDeleteData.user_id = this.userDetails.id;
    this.userDeleteData.token = this.userDetails.token;
  }
  ngOnInit() {
    this.dataSet = [];
    this.dataSetRe = [];
    this.dataSetFav = [];
    this.dataSetHide = [];
    this.dataSetPending = [];
    this.dataSetComp = [];
    this.dataSetCompRe = [];
    this.dataSetCompHide = [];
    this.dataSetCompPending = [];
    this.dataSetAsk = [];
    this.dataSetAskRe = [];
    this.dataSetAskHide = [];
    this.dataSetAskPending = [];
  }
  ionViewWillEnter() {
    this.common.presentLoading();
    this.getProduct();
    this.getCompany();
    this.getQuestion();
    this.common.closeLoading();
  }
  getProduct() {
    this.authService.postData(this.userPostData, "product").then((result) => {
        this.responseData = result;
        if (this.responseData.productData) {
          this.dataSet = this.responseData.productData;
          this.dataSetFav = this.responseData.productfav;
          this.dataSetRe = this.responseData.productRe;
          this.dataSet.custom = '';
          this.dataSet.checkbox_value = '';
          var i; var j; var k; var l;
          for (i = 0; i < this.dataSet.length; i++) {
            if (this.userDetails.name == "") {
              this.dataSet[i].name = this.userDetails.username;
            } else {
              this.dataSet[i].name = this.userDetails.name;
            }
            this.dataSet[i].username = this.userDetails.username;
            this.dataSet[i].userpic = "https://www.oukazon.com/storage/profile/small_"+this.userDetails.image;
            this.dataSet[i].screen_shot = this.dataSet[i].screen_shot.split(",");
            this.dataSet[i].tag = this.dataSet[i].tag.split(",");
            this.dataSet[i].latlong = this.dataSet[i].latlong.split(",");
            this.dataSet[i].lat = this.dataSet[i].latlong[0];
            this.dataSet[i].long = this.dataSet[i].latlong[1];
            this.dataSet[i].pictures = this.dataSet[i].screen_shot;
            this.dataSet[i].custom_types = this.dataSet[i].custom_types.split(",");
            this.dataSet[i].custom_values = this.dataSet[i].custom_values.split(",");
            this.dataSet[i].custom_fields = this.dataSet[i].custom_fields.split(",");
            for (l = 0; l < this.dataSet[i].custom_types.length; l++) {
              if (this.dataSet[i].custom_types[l] !== "checkbox"){
              this.dataSet[i].custom_types[l] = {type: this.dataSet[i].custom_types[l], title: this.dataSet[i].custom_fields[l], value: this.dataSet[i].custom_values[l]};
              } else {
                if (this.dataSet[i].custom_values[l] !== '') {this.dataSet[i].checkbox_title = this.dataSet[i].custom_fields[l];}
                this.dataSet[i].checkbox_value = this.dataSet[i].custom_values[l];
              }
            }
            if (this.dataSet[i].checkbox_value !== undefined){this.dataSet[i].checkbox_value = this.dataSet[i].checkbox_value.split("+");}
            else {this.dataSet[i].checkbox_value = '';}
            this.dataSet[i].custom = this.dataSet[i].custom_types;
            //this.dataSet[i].custom = this.custom;
            if (this.dataSet[i].status == 'hide') { this.dataSetHide.push(this.dataSet[i]);
              console.log(this.dataSetHide);
              console.log(this.dataSet[i].updated_at);
            }
            if (this.dataSet[i].status == 'pending') {this.dataSetPending.push(this.dataSet[i]);
              console.log(this.dataSetPending);
            }
          }
          for (j = 0; j < this.dataSetFav.length; j++) {
            this.dataSetFav[j].userpic = "https://www.oukazon.com/storage/profile/small_"+this.dataSetFav[j].image;
            this.dataSetFav[j].screen_shot = this.dataSetFav[j].screen_shot.split(",");
            this.dataSetFav[j].tag = this.dataSetFav[j].tag.split(",");
            this.dataSetFav[j].latlong = this.dataSetFav[j].latlong.split(",");
            this.dataSetFav[j].lat = this.dataSetFav[j].latlong[0];
            this.dataSetFav[j].long = this.dataSetFav[j].latlong[1];
            this.dataSetFav[j].pictures = this.dataSetFav[j].screen_shot;
            this.dataSetFav[j].custom_types = this.dataSetFav[j].custom_types.split(",");
            this.dataSetFav[j].custom_values = this.dataSetFav[j].custom_values.split(",");
            this.dataSetFav[j].custom_fields = this.dataSetFav[j].custom_fields.split(",");
            for (l = 0; l < this.dataSetFav[j].custom_types.length; l++) {
              if (this.dataSetFav[j].custom_types[l] !== "checkbox"){
              this.dataSetFav[j].custom_types[l] = {type: this.dataSetFav[j].custom_types[l], title: this.dataSetFav[j].custom_fields[l], value: this.dataSetFav[j].custom_values[l]};
              } else {
                if (this.dataSetFav[j].custom_values[l] !== '') {this.dataSetFav[j].checkbox_title = this.dataSetFav[j].custom_fields[l];}
                this.dataSetFav[j].checkbox_value = this.dataSetFav[j].custom_values[l];
              }
            }
            if (this.dataSetFav[j].checkbox_value !== undefined){this.dataSetFav[j].checkbox_value = this.dataSetFav[j].checkbox_value.split("+");}
            else {this.dataSetFav[j].checkbox_value = '';}
            this.dataSetFav[j].custom = this.dataSetFav[j].custom_types;
          }
          for (k = 0; k < this.dataSetRe.length; k++) {
            this.dataSetRe[k].userpic = "https://www.oukazon.com/storage/profile/small_"+this.dataSetRe[k].image;
            this.dataSetRe[k].screen_shot = this.dataSetRe[k].screen_shot.split(",");
            this.dataSetRe[k].tag = this.dataSetRe[k].tag.split(",");
            this.dataSetRe[k].latlong = this.dataSetRe[k].latlong.split(",");
            this.dataSetRe[k].lat = this.dataSetRe[k].latlong[0];
            this.dataSetRe[k].long = this.dataSetRe[k].latlong[1];
            this.dataSetRe[k].pictures = this.dataSetRe[k].screen_shot;
            this.dataSetRe[k].custom_types = this.dataSetRe[k].custom_types.split(",");
            this.dataSetRe[k].custom_values = this.dataSetRe[k].custom_values.split(",");
            this.dataSetRe[k].custom_fields = this.dataSetRe[k].custom_fields.split(",");
            for (l = 0; l < this.dataSetRe[k].custom_types.length; l++) {
              if (this.dataSetRe[k].custom_types[l] !== "checkbox"){
              this.dataSetRe[k].custom_types[l] = {type: this.dataSetRe[k].custom_types[l], title: this.dataSetRe[k].custom_fields[l], value: this.dataSetRe[k].custom_values[l]};
              } else {
                if (this.dataSetRe[k].custom_values[l] !== '') {this.dataSetRe[k].checkbox_title = this.dataSetRe[k].custom_fields[l];}
                this.dataSetRe[k].checkbox_value = this.dataSetRe[k].custom_values[l];
              }
            }
            if (this.dataSetRe[k].checkbox_value !== undefined){this.dataSetRe[k].checkbox_value = this.dataSetRe[k].checkbox_value.split("+");}
            else {this.dataSetRe[k].checkbox_value = '';}
            this.dataSetRe[k].custom = this.dataSetRe[k].custom_types;
          }
          console.log(this.dataSet);
          console.log(this.dataSetFav);
          console.log(this.dataSetRe);

        } else {
          console.log("no");
        }
      }, (err) => {

      });
  }

  getCompany() {
    this.authService.postData(this.userPostData, "company").then((result) => {
        this.responseData = result;
        if (this.responseData.companyData) {
          this.dataSetComp = this.responseData.companyData;
          this.dataSetCompRe = this.responseData.companyRe;
          this.dataSetComp.custom = '';
          this.dataSetComp.checkbox_value = '';
          var i;  var k; var l;
          for (i = 0; i < this.dataSetComp.length; i++) {
            if (this.userDetails.name == "") {
              this.dataSetComp[i].name = this.userDetails.username;
            } else {
              this.dataSetComp[i].name = this.userDetails.name;
            }
            this.dataSetComp[i].name = this.userDetails.name;
            this.dataSetComp[i].username = this.userDetails.username;
            this.dataSetComp[i].userpic = "https://www.oukazon.com/storage/profile/small_"+this.userDetails.image;
            this.dataSetComp[i].screen_shot = this.dataSetComp[i].screen_shot.split(",");
            this.dataSetComp[i].tag = this.dataSetComp[i].tag.split(",");
            this.dataSetComp[i].latlong = this.dataSetComp[i].latlong.split(",");
            this.dataSetComp[i].lat = this.dataSetComp[i].latlong[0];
            this.dataSetComp[i].long = this.dataSetComp[i].latlong[1];
            this.dataSetComp[i].pictures = this.dataSetComp[i].screen_shot;
            this.dataSetComp[i].custom_types = this.dataSetComp[i].custom_types.split(",");
            this.dataSetComp[i].custom_values = this.dataSetComp[i].custom_values.split(",");
            this.dataSetComp[i].custom_fields = this.dataSetComp[i].custom_fields.split(",");
            for (l = 0; l < this.dataSetComp[i].custom_types.length; l++) {
              if (this.dataSetComp[i].custom_types[l] !== "checkbox"){
              this.dataSetComp[i].custom_types[l] = {type: this.dataSetComp[i].custom_types[l], title: this.dataSetComp[i].custom_fields[l], value: this.dataSetComp[i].custom_values[l]};
              } else {
                if (this.dataSetComp[i].custom_values[l] !== '') {this.dataSetComp[i].checkbox_title = this.dataSetComp[i].custom_fields[l];}
                this.dataSetComp[i].checkbox_value = this.dataSetComp[i].custom_values[l];
              }
            }
            if (this.dataSetComp[i].status == 'hide') { this.dataSetCompHide.push(this.dataSetComp[i]);
              console.log(this.dataSetCompHide);
              console.log(this.dataSetComp[i].updated_at);
            }
            if (this.dataSetComp[i].status == 'pending') {this.dataSetCompPending.push(this.dataSetComp[i]);
              console.log(this.dataSetCompPending);
            }
          }
          for (k = 0; k < this.dataSetCompRe.length; k++) {
            this.dataSetCompRe[k].userpic = "https://www.oukazon.com/storage/profile/small_"+this.dataSetCompRe[k].image;
            this.dataSetCompRe[k].screen_shot = this.dataSetCompRe[k].screen_shot.split(",");
            this.dataSetCompRe[k].tag = this.dataSetCompRe[k].tag.split(",");
            this.dataSetCompRe[k].latlong = this.dataSetCompRe[k].latlong.split(",");
            this.dataSetCompRe[k].lat = this.dataSetCompRe[k].latlong[0];
            this.dataSetCompRe[k].long = this.dataSetCompRe[k].latlong[1];
            this.dataSetCompRe[k].pictures = this.dataSetCompRe[k].screen_shot;
            this.dataSetCompRe[k].custom_types = this.dataSetCompRe[k].custom_types.split(",");
            this.dataSetCompRe[k].custom_values = this.dataSetCompRe[k].custom_values.split(",");
            this.dataSetCompRe[k].custom_fields = this.dataSetCompRe[k].custom_fields.split(",");
            for (l = 0; l < this.dataSetCompRe[k].custom_types.length; l++) {
              if (this.dataSetCompRe[k].custom_types[l] !== "checkbox"){
              this.dataSetCompRe[k].custom_types[l] = {type: this.dataSetCompRe[k].custom_types[l], title: this.dataSetCompRe[k].custom_fields[l], value: this.dataSetCompRe[k].custom_values[l]};
              } else {
                if (this.dataSetCompRe[k].custom_values[l] !== '') {this.dataSetCompRe[k].checkbox_title = this.dataSetCompRe[k].custom_fields[l];}
                this.dataSetCompRe[k].checkbox_value = this.dataSetCompRe[k].custom_values[l];
              }
            }
            if (this.dataSetCompRe[k].checkbox_value !== undefined){this.dataSetCompRe[k].checkbox_value = this.dataSetCompRe[k].checkbox_value.split("+");}
            else {this.dataSetCompRe[k].checkbox_value = '';}
            this.dataSetCompRe[k].custom = this.dataSetCompRe[k].custom_types;
          }
          console.log(this.dataSetComp);
          console.log(this.dataSetCompRe);
        } else {
          console.log("no");
        }
      }, (err) => {

      });
  }

  getQuestion() {
    this.authService.postData(this.userPostData, "question").then((result) => {
        this.responseData = result;
        if (this.responseData.questionData) {
          this.dataSetAsk = this.responseData.questionData;
          this.dataSetAskRe = this.responseData.questionRe;
          var i;  var k;
          for (i = 0; i < this.dataSetAsk.length; i++) {
            if (this.userDetails.name == "") {
              this.dataSetAsk[i].name = this.userDetails.username;
            } else {
              this.dataSetAsk[i].name = this.userDetails.name;
            }
            this.dataSetAsk[i].username = this.userDetails.username;
            this.dataSetAsk[i].userpic = "https://www.oukazon.com/storage/profile/small_"+this.userDetails.image;
            this.dataSetAsk[i].screen_shot = this.dataSetAsk[i].screen_shot.split(",");
            this.dataSetAsk[i].tag = this.dataSetAsk[i].tag.split(",");
            this.dataSetAsk[i].latlong = this.dataSetAsk[i].latlong.split(",");
            this.dataSetAsk[i].lat = this.dataSetAsk[i].latlong[0];
            this.dataSetAsk[i].long = this.dataSetAsk[i].latlong[1];
            this.dataSetAsk[i].pictures = this.dataSetAsk[i].screen_shot;
            if (this.dataSetAsk[i].status == 'hide') { this.dataSetAskHide.push(this.dataSetAsk[i]);
              console.log(this.dataSetAskHide);
              console.log(this.dataSetAsk[i].updated_at);
            }
            if (this.dataSetAsk[i].status == 'pending') {this.dataSetAskPending.push(this.dataSetAsk[i]);
              console.log(this.dataSetAskPending);
            }
          }
          for (k = 0; k < this.dataSetAskRe.length; k++) {
            this.dataSetAskRe[k].userpic = "https://www.oukazon.com/storage/profile/small_"+this.dataSetAskRe[k].image;
            this.dataSetAskRe[k].screen_shot = this.dataSetAskRe[k].screen_shot.split(",");
            this.dataSetAskRe[k].tag = this.dataSetAskRe[k].tag.split(",");
            this.dataSetAskRe[k].latlong = this.dataSetAskRe[k].latlong.split(",");
            this.dataSetAskRe[k].lat = this.dataSetAskRe[k].latlong[0];
            this.dataSetAskRe[k].long = this.dataSetAskRe[k].latlong[1];
            this.dataSetAskRe[k].pictures = this.dataSetAskRe[k].screen_shot;
          }
          console.log(this.dataSet);
          console.log(this.dataSetAskRe);
        } else {
          console.log("no");
        }
      }, (err) => {

      });
  }
  openAd(msgIndex){this.navCtrl.push(AdDetailPage, {"product":this.dataSet[msgIndex]});}
  openHiddenAd(msgIndex){this.navCtrl.push(AdDetailPage, {"product":this.dataSetHide[msgIndex]});}
  openPendingAd(msgIndex){this.navCtrl.push(AdDetailPage, {"product":this.dataSetPending[msgIndex]});}
  openFavAd(msgIndex){this.navCtrl.push(AdDetailPage, {"product":this.dataSetFav[msgIndex]});}
  openReAd(msgIndex){this.navCtrl.push(AdDetailPage, {"product":this.dataSetRe[msgIndex]});}

  c_showAd() {if (this.showAd==false) {this.showAd = true} else {this.showAd = false}}
  c_showAd1() {if (this.showAd1==false) {this.showAd1 = true} else {this.showAd1 = false}}
  c_showAd2() {if (this.showAd2==false) {this.showAd2 = true} else {this.showAd2 = false}}
  c_showAd3() {if (this.showAd3==false) {this.showAd3 = true} else {this.showAd3 = false}}
  c_showAd4() {if (this.showAd4==false) {this.showAd4 = true} else {this.showAd4 = false}}

  openComp(msgIndex){this.navCtrl.push(CompDetailPage, {"company":this.dataSetComp[msgIndex]});}
  openHiddenComp(msgIndex){this.navCtrl.push(CompDetailPage, {"company":this.dataSetCompHide[msgIndex]});}
  openPendingComp(msgIndex){this.navCtrl.push(CompDetailPage, {"company":this.dataSetCompPending[msgIndex]});}
  openReComp(msgIndex){this.navCtrl.push(CompDetailPage, {"company":this.dataSetCompRe[msgIndex]});}

  c_showComp() {if (this.showComp==false) {this.showComp = true} else {this.showComp = false}}
  c_showComp2() {if (this.showComp2==false) {this.showComp2 = true} else {this.showComp2 = false}}
  c_showComp3() {if (this.showComp3==false) {this.showComp3 = true} else {this.showComp3 = false}}
  c_showComp4() {if (this.showComp4==false) {this.showComp4 = true} else {this.showComp4 = false}}

  openAsk(msgIndex){this.navCtrl.push(AskDetailPage, {"question":this.dataSetAsk[msgIndex]});}
  openHiddenAsk(msgIndex){this.navCtrl.push(AskDetailPage, {"question":this.dataSetAskHide[msgIndex]});}
  openPendingAsk(msgIndex){this.navCtrl.push(AskDetailPage, {"question":this.dataSetAskPending[msgIndex]});}
  openReAsk(msgIndex){this.navCtrl.push(AskDetailPage, {"question":this.dataSetAskRe[msgIndex]});}

  c_showAsk() {if (this.showAsk==false) {this.showAsk = true} else {this.showAsk = false}}
  c_showAsk2() {if (this.showAsk2==false) {this.showAsk2 = true} else {this.showAsk2 = false}}
  c_showAsk3() {if (this.showAsk3==false) {this.showAsk3 = true} else {this.showAsk3 = false}}
  c_showAsk4() {if (this.showAsk4==false) {this.showAsk4 = true} else {this.showAsk4 = false}}

  edit(msgIndex) {this.navCtrl.push(EditAdPage, {"product":this.dataSet[msgIndex]});}
  edith(msgIndex) {this.navCtrl.push(EditAdPage, {"product":this.dataSetHide[msgIndex]});}

  editComp(msgIndex) {this.navCtrl.push(EditCompPage, {"company":this.dataSetComp[msgIndex]});}
  edithComp(msgIndex) {this.navCtrl.push(EditCompPage, {"company":this.dataSetCompHide[msgIndex]});}

  editAsk(msgIndex) {this.navCtrl.push(EditAskPage, {"question":this.dataSetAsk[msgIndex]});}
  edithAsk(msgIndex) {this.navCtrl.push(EditAskPage, {"question":this.dataSetAskHide[msgIndex]});}

  convertTime(created_at) {
    let date = new Date(created_at * 1000);
    return date;
  }

  ProductDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete AD",
        message: "Do you want to Delete this AD?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "productDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSet.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  companyDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Company",
        message: "Do you want to Delete this Company?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "companyDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetComp.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  questionDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete question",
        message: "Do you want to Delete this Question?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "questionDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetAsk.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  ProductHideDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Hidden AD",
        message: "Do you want to Delete this Hidden AD?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "productDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSet.splice(msgIndex, 1);
                    this.dataSetHide.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  companyHideDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Hidden Company",
        message: "Do you want to Delete this Hidden Company?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "companyDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetComp.splice(msgIndex, 1);
                    this.dataSetCompHide.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  questionHideDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Hidden Question",
        message: "Do you want to Delete this Hidden Question?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "questionDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetAsk.splice(msgIndex, 1);
                    this.dataSetAskHide.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  ProductPendingDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Pending AD",
        message: "Do you want to Delete this Pending AD?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "productDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSet.splice(msgIndex, 1);
                    this.dataSetPending.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  companyPendingDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Pending Company",
        message: "Do you want to Delete this Pending Company?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "companyDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetComp.splice(msgIndex, 1);
                    this.dataSetCompPending.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  questionPendingDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Pending Question",
        message: "Do you want to Delete this Pending Question?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "questionDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetAsk.splice(msgIndex, 1);
                    this.dataSetAskPending.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  ProductFavDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Favorite AD",
        message: "Do you want to Delete this Favorite AD?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "favDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetFav.splice(msgIndex, 1);
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  ProductReSubmittedDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete ReSubmitted AD",
        message: "Do you want to Delete this ReSubmitted AD?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "ReSubmittedDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetRe.splice(msgIndex, 1);
                    this.getProduct();
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  companyReSubmittedDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Resubmitted Company",
        message: "Do you want to Delete this Resubmitted Company?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "ReSubmittedCompDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetCompRe.splice(msgIndex, 1);
                    this.getCompany();
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  questionReSubmittedDelete(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Resubmitted Question",
        message: "Do you want to Delete this Resubmitted Question?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "ReSubmittedAskDelete").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetAskRe.splice(msgIndex, 1);
                    this.getQuestion();
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  ProductHide(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Hide AD",
        message: "Do you want to Hide this AD? , People will not see this Ad anymore",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Hide",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "adHide").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetHide = [];
                    this.dataSetPending = [];
                    this.getProduct();
                    console.log("success");
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  companyHide(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Hide Company",
        message: "Do you want to Hide this Company? , People will not see this Company anymore",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Hide",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "compHide").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetCompHide = [];
                    this.dataSetCompPending = [];
                    this.getCompany();
                    console.log("success");
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  questionHide(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Hide Question",
        message: "Do you want to Hide this Question? , People will not see this Question anymore",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Hide",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "askHide").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetAskHide = [];
                    this.dataSetAskPending = [];
                    this.getQuestion();
                    console.log("success");
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  ProductShow(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Show AD",
        message: "Do you want to Show this AD? , This Ad will be Public",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Show",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "adShow").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetHide = [];
                    this.dataSetPending = [];
                    this.getProduct();
                    console.log("success");
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  companyShow(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Show Company",
        message: "Do you want to Show this Company? , This Ad will be Public",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Show",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "compShow").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetCompHide = [];
                    this.dataSetCompPending = [];
                    this.getCompany();
                    console.log("success");
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  questionShow(id, msgIndex) {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Show Question",
        message: "Do you want to Show this Question? , This Ad will be Public",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Show",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "askShow").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetAskHide = [];
                    this.dataSetAskPending = [];
                    this.getQuestion();
                    console.log("success");
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }

  ProductRepost(updated, id, msgIndex) {
    var date = new Date(updated);
    date.setDate(date.getDate() + 7);
    var date1 = new Date();
    if(date1 < date) {
      let alert = this.alertCtrl.create({
        title: 'Sorry!',
        subTitle: 'You can repost your Ad after 7 Days',
        buttons: ['Done']
      });
      alert.present();
    }
    else {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Repost AD",
        message: "Do you want to Repost this AD? ",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Repost",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "adRepost").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetHide = [];
                    this.dataSetPending = [];
                    this.getProduct();
                    console.log("success");
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }
  }

  questionRepost(updated, id, msgIndex) {
    var date = new Date(updated);
    date.setDate(date.getDate() + 1);
    var date1 = new Date();
    if(date1 < date) {
      let alert = this.alertCtrl.create({
        title: 'Sorry!',
        subTitle: 'You can repost your Question after 24 Hours',
        buttons: ['Done']
      });
      alert.present();
    }
    else {
    if (id > 0) {
      let alert = this.alertCtrl.create({
        title: "Repost Question",
        message: "Do you want to Repost this Question? ",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Repost",
            handler: () => {
              this.userDeleteData.id = id;
              this.authService.postData(this.userDeleteData, "askRepost").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.success) {
                    this.dataSetAskHide = [];
                    this.dataSetAskPending = [];
                    this.getQuestion();
                    console.log("success");
                  } else {
                    console.log("No access");
                  }
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }
  }





}
