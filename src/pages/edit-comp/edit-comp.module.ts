import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditCompPage } from './edit-comp';

@NgModule({
  declarations: [
    EditCompPage,
  ],
  imports: [
    IonicPageModule.forChild(EditCompPage),
  ],
})
export class EditCompPageModule {}
