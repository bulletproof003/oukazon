import { Component, ViewChild, ElementRef } from '@angular/core';
import { Oukazon } from '../../app/app.component';
import { IonicPage, NavController, NavParams, AlertController ,ToastController } from 'ionic-angular';
import { SMS } from '@ionic-native/sms';
import { AuthService } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable } from 'rxjs/Rx';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})

export class SignupPage {
  @ViewChild('time') mapElement: ElementRef;
  responseData : any;
  code : any;
  userData = {"username": "","password_hash": "", "name": "","email": "","phone": ""};
  authForm : FormGroup;
  codeForm : FormGroup;
  emailForm : FormGroup;
  step1: boolean;
  step2: boolean;
  step3: boolean;
  callDuration: any;
  text = {
    "number": "", 
    "message": "",
  };

  constructor(private elementRef: ElementRef, public alertCtrl: AlertController, private fb: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, private toastCtrl:ToastController, private sms: SMS) {
    this.authForm = this.fb.group({
		  //'name' : [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20)])],
		  'username' : [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20)])],
		  'phone' : [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(8)])],
      'password_hash': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern('^(?=.*?[a-z])(?=.*?[0-9]).{4,20}$')])],
      'password_hash2': [null, Validators.required],
    }, {validator: this.matchingPasswords('password_hash', 'password_hash2')});

    this.codeForm = this.fb.group({
		  'codes' : [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(6)])],
    });

    this.emailForm = this.fb.group({
      'name' : [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20)])],
		  'email': [null, Validators.compose([Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')])],
    });
    
    

    

  }
  ngOnInit() {
    this.step1 = true;
    this.step2 = false;
    this.step3 = false;
    
  }

  startTimer(display) {
    var timer = 300;
    var minutes;
    var seconds;

    var tic = Observable.interval(1000).subscribe(x => {
        minutes = Math.floor(timer / 60);
        seconds = Math.floor(timer % 60);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        --timer;
        if (--timer < 0) {
             console.log('timeup');
             this.navCtrl.setRoot(Oukazon);
             tic.unsubscribe();
        }
    })
    
}

  matchingPasswords(password_hash: string, password_hash2: string) {
    return (authForm: FormGroup): {[key: string]: any} => {
      let password = authForm.controls[password_hash];
      let confirmPassword = authForm.controls[password_hash2];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  info2() {
    let alert = this.alertCtrl.create({
      title: 'SMS Validation!',
      subTitle: 'Thank You, For Using OUKAZON App!, In case the Code Is Wrong this page will be Closed Automatically.',
      buttons: ['OK']
    });
    alert.present();
  }

  info1() {
    let alert = this.alertCtrl.create({
      title: 'SMS Validation!',
      subTitle: 'Thank You, For Using OUKAZON App!, Once you use your mobile phone number for SMS Validation, You will be charged of the SMS Cost following your communication provider rates, If there is an Issue sending the SMS, You should provide Your Full Name and Your Email, to create an account. We clear our part for any misunderstanding',
      buttons: ['OK']
    });
    alert.present();
  }

gostep2(){
  this.step1 = false;
  this.step2 = true;
  this.step3 = false;
  setTimeout(() => {
    this.callDuration = this.elementRef.nativeElement.querySelector("#time");
  this.startTimer(this.callDuration);
  }, 500);
}

gostep3(){
  this.step1 = false;
  this.step2 = false;
  this.step3 = true;
}

async send() {
  var options: {
    replaceLineBreaks: true,
    android: {
      intent: 'INTENT'
    }
  }
  await this.sms.send(this.text.number, this.text.message, options).then((result) => {
  let successToast = this.toastCtrl.create({
    message: "SMS Validation CODE sent successfully",
    duration: 3000,
    position: 'top'
  });
  successToast.present();
  this.gostep2();
  console.log('call OK')
  }, (error) => {
    let errorToast = this.toastCtrl.create({
      message: "SMS Validation CODE not sent. :(",
      duration: 3000,
      position: 'top'
    });
    errorToast.present();
    this.gostep3();
  });

}

coding() {
  this.code = 100000 + Math.floor(Math.random() * 900000);
}



smsForm(value: any):void{
  this.coding();
  this.userData.email = '';
  this.userData.name = '';
  this.userData.username = value.username;
  this.userData.password_hash = value.password_hash;
  this.userData.phone = value.phone;
  this.text.number = "00974"+this.userData.phone;
  this.text.message = "Ouka-"+this.code+": is your OUKAZON Verification Code. Valid For 5 Mins";
  this.send();
}

 submitForm(value: any):void{
   if (value.codes != this.code) {
    this.navCtrl.setRoot(Oukazon);
  } else {
  this.authService.postData(this.userData,"signup").then((result) => {
    this.responseData = result;
    if(this.responseData.userData){
    console.log(this.responseData.userData);
    localStorage.setItem('userData', JSON.stringify(this.responseData));
    this.navCtrl.setRoot(Oukazon);
    }
    else {
      this.presentToast("USERNAME OR EMAIL ALREADY USED");
      }
  }, (err) => {
    this.presentToast("USERNAME OR EMAIL ALREADY USED");
  });

  
}

  console.log(value);
}

presentToast(msg) {
  let toast = this.toastCtrl.create({
    message: msg,
    duration: 5000,
    position: 'top'
  });
  toast.present();
}

  login(){
    this.navCtrl.push(LoginPage);
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

}

