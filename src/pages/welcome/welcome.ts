import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { Oukazon } from '../../app/app.component';


@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if(localStorage.getItem('userData')){
    this.navCtrl.setRoot(HomePage);
    }
  }

  signup(){
  this.navCtrl.push(SignupPage);
  }
  login(){
  this.navCtrl.push(LoginPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

}
