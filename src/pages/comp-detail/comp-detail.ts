import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthService } from "../../providers/auth-service/auth-service";
import { ProductService } from '../../providers/product-service';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Common } from "../../providers/common";
import { CallNumber } from '@ionic-native/call-number';
import { Chat } from '../chat/chat';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-comp-detail',
  templateUrl: 'comp-detail.html',
})
export class CompDetailPage {
  @ViewChild('compmap') compmapRef:ElementRef;
  @ViewChild("commentboxcomp") commentboxcomp;
  @ViewChild("idbox") idbox;
  @ViewChild("userbox") userbox;

  public userDetails: any;
  public responseData: any;
  public resposeData: any;
  public resposeDataLike: any;
  public dataSet: any;
  public dataCount: any;
  myGroup: any;
  userPostDataComp = {
    id: "",
    text: "",
    company_id: "",
    userimage: "",
    username: "",
    token: "",
  };
  updateView = {
    id: "",
    company_id: "",
    token: "",
  };
  userPostLike = {
    u_id: "",
    user_comment_id: "",
    ids: "",
    company_id: "",
    token: "",
    amount: "",
  };
  msgs1 = {
    id: "",
    username: "",
    name: "",
  };
  payments: any;
  langData: any;
  like_reward: any;
  comp: any;
  google: any;
  comments : any;
  cmnt : any;
  like : any;

  constructor(private productService: ProductService, private iab: InAppBrowser, public alertCtrl: AlertController, private callNumber: CallNumber, public common: Common, public authService: AuthService, public navCtrl: NavController, public navParams: NavParams) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.langData = JSON.parse(localStorage.getItem('clang'));
    this.userDetails = data.userData;
    this.userPostDataComp.id = this.userDetails.id;
    this.userPostLike.u_id = this.userDetails.id;
    this.userPostLike.u_id = this.userDetails.id;
    this.userPostLike.token = this.userDetails.token;
    this.userPostDataComp.token = this.userDetails.token;
    this.updateView.id = this.userDetails.id;
    this.updateView.token = this.userDetails.token;
    this.userPostDataComp.userimage = this.userDetails.image;
    this.userPostDataComp.username = this.userDetails.username;
    this.comp = this.navParams.get("company");
    this.userPostLike.company_id = this.comp.id;
    this.updateView.company_id = this.comp.id;
    this.userPostDataComp.company_id = this.comp.id;
    this.userPostLike.company_id = this.comp.id;
    this.msgs1.id = this.comp.user_id;
    this.msgs1.username = this.comp.username;
    this.msgs1.name = this.comp.name;
    this.authService.postData(this.updateView, "companyview")
    this.getComments();
  }
  
  getComments() {
    var aaa = [];
    this.authService.postData(this.userPostDataComp, "commentsComp").then((result) => {
        this.responseData = result;
        if (this.responseData.commentsDataComp) {
          this.dataSet = this.responseData.commentsDataComp;
          this.dataCount = this.dataSet.length;
          var xxx = this.responseData.likesData;
          //this.dataSet.push(xxx);
          this.dataSet.user_id = '';
          for (let i = 0; i < this.dataSet.length; i++) {
            for (let l = 0; l < xxx.length; l++) {
              if (this.dataSet[i].id === xxx[l].comment_id) {
                aaa.push(xxx[l].user_id);
                this.dataSet[i].user_id = aaa;
              }
            }
            aaa = []
          }
        
          for (let i = 0; i < this.dataSet.length; i++) {
            this.dataSet[i].index = false;
            if (this.dataSet[i].user_id != undefined) {
          var qqq = this.dataSet[i].user_id;
          var ttt;
          
            for (let m = 0; m < qqq.length; m++) {
              if (this.userDetails.id == qqq[m]) {
                ttt = true;
                this.dataSet[i].index = ttt;
              }
            }
            ttt = '';
            qqq = [];
          } 
          }
          
          console.log(this.dataCount);
          console.log(this.dataSet);
          console.log(xxx);
        } else {
          console.log("no");
        }
      }, (err) => {
      });
  }

  makecall() {
    this.callNumber.callNumber("00974"+this.comp.landline, true)
  .then(() => console.log('Launched dialer!'))
  .catch(() => alert('Error launching dialer'));
  }

  chat() {
    this.navCtrl.push(Chat, {"toUser":this.msgs1});
  }

  showfax() {
    let alert = this.alertCtrl.create({
      title: 'Fax Number',
      subTitle: this.comp.fax,
      buttons: ['OK']
    });
    alert.present();
  }

  visitweb(){
    var pattern = /^((http|https|ftp):\/\/)/;
    if(!pattern.test(this.comp.web)) {
      this.comp.web = "http://" + this.comp.web;
      this.iab.create(this.comp.web);
    } else {
      this.iab.create(this.comp.web);
    }
  }

  showemail() {
    let alert = this.alertCtrl.create({
      title: 'E-mail',
      subTitle: this.comp.email,
      buttons: ['OK']
    });
    alert.present();
  }

  ngOnInit() {
    this.getPayment();
    this.comp = this.navParams.get("company");
    const compLocation = new google.maps.LatLng(this.comp.lat,this.comp.long);
    const options = {
      center:compLocation,
      styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }],
      zoom:14,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    };
    const map = new google.maps.Map(this.compmapRef.nativeElement,options);
    this.addMarker(compLocation,map);
  }
  
  addMarker(position,map) {
    return new google.maps.Marker({
      position,
      map,
      icon: {
        url: './assets/icon/marker.png'
    }
    })
  }

  postComment() {
    if (this.userPostDataComp.text) {
      this.common.presentLoading();
      this.authService.postData(this.userPostDataComp, "postCommentComp").then(
        result => {
          this.resposeData = result;
          this.cmnt = this.resposeData.commentDataComp;
          this.cmnt.username = this.userPostDataComp.username;
          if (this.cmnt) {
            this.common.closeLoading();
            this.dataSet.unshift(this.cmnt);
            this.userPostDataComp.text = "";

            //this.commentbox.setFocus();
            setTimeout(() => {
              //  this.commentbox.focus();
            }, 150);
          } else {
            console.log("No access");
          }
        },
        err => {
          //Connection failed message
        }
      );
    }
  }

  set_ids(i) {
    this.common.presentLoading();
    this.userPostLike.ids = this.dataSet[i].id;
    this.userPostLike.user_comment_id = this.dataSet[i].user_comment_id;
    this.userPostLike.amount = this.like_reward;
    this.likesComment();
    this.common.closeLoading();
    this.getComments();
  }

  getPayment() {
    this.productService.getPayments().subscribe(
      datax => {
        this.payments = datax.payment;
        this.like_reward = this.payments[0].like_reward;
        console.log(this.payments);
        console.log(this.like_reward);
    },
      error =>{
        console.log("error Get Payment");
    });
  }

  likesComment() {
      if (this.userPostLike) {
        this.authService.postData(this.userPostLike, "likecmntscomp")
      .then(result => {
      this.resposeDataLike = result;
      //this.like = this.resposeDataLike.likeData;
      var xx = this.resposeDataLike.success;
      console.log(xx);
        }, err => {
          var b = JSON.stringify(err);  //Connection failed message
          console.log(b);
        }
      );
    } else {
      console.log("Problem Like");
    }
  }

  del_ids(i) {
    this.common.presentLoading();
    this.userPostLike.ids = this.dataSet[i].id;
    this.userPostLike.user_comment_id = this.dataSet[i].user_comment_id;
    this.userPostLike.amount = this.like_reward;
    this.unlikesComment();
    this.common.closeLoading();
    this.getComments();
  }


  unlikesComment() {
      if (this.userPostLike) {
        this.authService.postData(this.userPostLike, "unlikecmntscomp")
      .then(result => {
      this.resposeDataLike = result;
      //this.like = this.resposeDataLike.likeData;
      var xx = this.resposeDataLike.success;
      console.log(xx);
        }, err => {
          var b = JSON.stringify(err);  //Connection failed message
          console.log(b);
        }
      );
    } else {
      console.log("Problem Like");
    }
    
  }

}