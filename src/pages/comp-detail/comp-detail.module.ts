import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompDetailPage } from './comp-detail';

@NgModule({
  declarations: [
    CompDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CompDetailPage),
  ],
})
export class CompDetailPageModule {}
