import { Component } from '@angular/core';
import { App, NavController, NavParams, Platform } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Common } from '../../providers/common';
import { CompDetailPage } from '../comp-detail/comp-detail';
import { HomePage } from '../home/home';
import { ClassifiedPage } from '../classified/classified';
import { AskPage } from '../ask/ask';

@Component({
  selector: 'page-directory',
  templateUrl: 'directory.html'
})
export class DirectoryPage {
  catDetails = { "userid": "", "token": "" };
  catData: any;
  catList: any;
  public userDetails: any;

  subcat: any;
  subcatDetails = { "userid": "", "token": "", "cat_id": "" };
  subcatData: any;
  subcatList: any;
  title: any;

  step1: boolean;
  step2: boolean;
  step3: boolean;

  companies: any;
  companiesData: any;
  noRecordsComp: boolean;
  responseDataComp: any;
  langData: any;
  userPostDataComp = { 
    id: "", 
    token: "",
    updated_at: "",
    cat_id: "",
    sub_cat_id: "",
  };


  constructor(private app: App, public authService: AuthService, public platform: Platform, public common: Common, public navCtrl: NavController, public navParams: NavParams) {
    this.langData = JSON.parse(localStorage.getItem('clang'));
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data.userData;
    this.catDetails.userid = this.userDetails.id;
    this.catDetails.token = this.userDetails.token;
    this.subcatDetails.userid = this.userDetails.id;
    this.subcatDetails.token = this.userDetails.token;
    

  }

  yourMethod(): void {
    this.app.getRootNav().setRoot(HomePage);
  }

  ionViewWillEnter() {

    this.common.presentLoading();
    this.step1 = true;
    this.step2 = false;
    this.step3 = false;
    this.userPostDataComp.updated_at = '';
    this.noRecordsComp = false;
    this.getCat();
    this.catData = [];
    this.subcatData = [];
    this.subcatList = [];
    this.responseDataComp = [];
    this.companiesData = [];
    this.companies = [];
    this.common.closeLoading();
  }

  showComp(i) {
    this.navCtrl.push(CompDetailPage, { "company": this.companies[i] });
  }

  pushC() {
    this.navCtrl.push(ClassifiedPage);
  }

  pushA() {
    this.navCtrl.push(AskPage);
  }

  getCat() {
    this.authService.postData(this.catDetails, "companycat").then((result) => {
      this.catData = result;
      if (this.catData.getCompCatList) {
        this.catList = this.catData.getCompCatList;
        console.log(this.catList);
      }
      else {
        console.log("problem categories");
      }
    }, (err) => {

    });
  }

  getsubCat() {
    
    this.authService.postData(this.subcatDetails, "companysubcat").then((result) => {
      this.subcatData = result;
      if (this.subcatData.getCompSubCatList) {
        this.subcatList = this.subcatData.getCompSubCatList;
        console.log(this.subcatList);
      }
      else {
        console.log("problem sub_categories");
      }
    }, (err) => {

    });
  }

  adSubCat(i){
    this.common.presentLoading();
    this.step1 = false;
    this.step2 = true;
    this.subcat = this.catList[i];
    console.log(this.subcat);
    console.log(this.catList[i]);
    this.subcatDetails.cat_id = this.subcat.cat_id;
    this.getsubCat();
    this.common.closeLoading();
  }

  postadSubCat(i){
    this.userPostDataComp.id = this.userDetails.id;
    this.userPostDataComp.token = this.userDetails.token;
    this.userPostDataComp.cat_id = this.subcatList[i].main_cat_id;
    this.userPostDataComp.sub_cat_id = this.subcatList[i].sub_cat_id;
    this.title = this.subcatList[i].sub_cat_name;
    console.log(this.subcatList[i]);
    this.step2 = false;
    this.step3 = true;
    this.getCompany();

  }

  backToCat(){
    this.common.presentLoading();
    this.subcat = [];
    this.subcatList = [];
    this.step1 = true;
    this.step2 = false;
    this.step3 = false;
    this.common.closeLoading();
  }

  backToSubCat(){
    this.common.presentLoading();
    this.getsubCat();
    this.userPostDataComp.updated_at = '';
    this.noRecordsComp = false;
    this.companies = [];
    this.companiesData = [];
    this.step2 = true;
    this.step3 = false;
    this.common.closeLoading();
  }

  doRefresh(refresher) {
    this.userPostDataComp.updated_at = '';
    this.noRecordsComp = false;
    this.companies = [];
    this.companiesData = [];

    this.getCompany();


    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }


  getCompany() {
    this.authService.postData(this.userPostDataComp, "companies1").then((result) => {
      this.responseDataComp = result;
      if (this.responseDataComp.companyData0) {
        this.companies = this.responseDataComp.companyData0;
        this.userPostDataComp.updated_at = this.companies[this.companies.length -1].updated_at;
        console.log("Updated At " + this.userPostDataComp.updated_at);
        this.companies.custom = '';
        this.companies.checkbox_value = '';
        var i; var l;
        for (i = 0; i < this.companies.length; i++) {
          this.companies[i].userpic = "https://www.oukazon.com/storage/profile/small_" + this.companies[i].image;
          this.companies[i].screen_shot = this.companies[i].screen_shot.split(",");
          this.companies[i].tag = this.companies[i].tag.split(",");
          this.companies[i].latlong = this.companies[i].latlong.split(",");
          this.companies[i].lat = this.companies[i].latlong[0];
          this.companies[i].long = this.companies[i].latlong[1];
          this.companies[i].pictures = this.companies[i].screen_shot;
          this.companies[i].screen_shot = "https://www.oukazon.com/storage/companies/screenshot/small_" + this.companies[i].screen_shot[0];
          this.companies[i].custom_types = this.companies[i].custom_types.split(",");
          this.companies[i].custom_values = this.companies[i].custom_values.split(",");
          this.companies[i].custom_fields = this.companies[i].custom_fields.split(",");
          for (l = 0; l < this.companies[i].custom_types.length; l++) {
            if (this.companies[i].custom_types[l] !== "checkbox") {
              this.companies[i].custom_types[l] = { type: this.companies[i].custom_types[l], title: this.companies[i].custom_fields[l], value: this.companies[i].custom_values[l] };
            } else {
              if (this.companies[i].custom_values[l] !== '') { this.companies[i].checkbox_title = this.companies[i].custom_fields[l]; }
              this.companies[i].checkbox_value = this.companies[i].custom_values[l];
            }
          }
          if (this.companies[i].checkbox_value !== undefined) { this.companies[i].checkbox_value = this.companies[i].checkbox_value.split("+"); }
          else { this.companies[i].checkbox_value = ''; }
          this.companies[i].custom = this.companies[i].custom_types;
        }
        console.log(this.companies);
      } else {
        console.log("no");
      }
    }, (err) => {

    });
  }

  doInfiniteComp(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      
      this.authService.postData(this.userPostDataComp, "companies1").then((result) => {
        this.responseDataComp = result;
        if (this.responseDataComp.companyData0.length) {
          this.companiesData = this.responseDataComp.companyData0;
          this.userPostDataComp.updated_at = this.companiesData[this.companiesData.length - 1].updated_at;
          var a; var b;
          
          this.companiesData.custom = '';
          this.companiesData.checkbox_value = '';
          for (a = 0; a < this.companiesData.length; a++) {
            this.companiesData[a].userpic = "https://www.oukazon.com/storage/profile/small_" + this.companiesData[a].image;
            this.companiesData[a].screen_shot = this.companiesData[a].screen_shot.split(",");
            this.companiesData[a].tag = this.companiesData[a].tag.split(",");
            this.companiesData[a].latlong = this.companiesData[a].latlong.split(",");
            this.companiesData[a].lat = this.companiesData[a].latlong[0];
            this.companiesData[a].long = this.companiesData[a].latlong[1];
            this.companiesData[a].pictures = this.companiesData[a].screen_shot;
            this.companiesData[a].screen_shot = "https://www.oukazon.com/storage/companies/screenshot/small_" + this.companiesData[a].screen_shot[0];
            this.companiesData[a].custom_types = this.companiesData[a].custom_types.split(",");
            this.companiesData[a].custom_values = this.companiesData[a].custom_values.split(",");
            this.companiesData[a].custom_fields = this.companiesData[a].custom_fields.split(",");
            for (b = 0; b < this.companiesData[a].custom_types.length; b++) {
              if (this.companiesData[a].custom_types[b] !== "checkbox") {
                this.companiesData[a].custom_types[b] = { type: this.companiesData[a].custom_types[b], title: this.companiesData[a].custom_fields[b], value: this.companiesData[a].custom_values[b] };
              } else {
                if (this.companiesData[a].custom_values[b] !== '') { this.companiesData[a].checkbox_title = this.companiesData[a].custom_fields[b]; }
                this.companiesData[a].checkbox_value = this.companiesData[a].custom_values[b];
              }
            }
            if (this.companiesData[a].checkbox_value !== undefined) { this.companiesData[a].checkbox_value = this.companiesData[a].checkbox_value.split("+"); }
            else { this.companiesData[a].checkbox_value = ''; }
            this.companiesData[a].custom = this.companiesData[a].custom_types;
          }

          for (let m = 0; m < this.companiesData.length; m++) {
            this.companies.push(this.companiesData[m]);
          }
          console.log(this.companies);
        } else {
          this.noRecordsComp = true;
        }
      }, (err) => {
  
      });
      
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }






}
