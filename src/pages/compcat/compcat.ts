import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { CompsubcatPage } from '../compsubcat/compsubcat';

@Component({
  selector: 'page-compcat',
  templateUrl: 'compcat.html',
})
export class CompcatPage {
  catDetails = { "userid": "", "token": "" };
  catData: any;
  catList: any;
  userDetails: any;
  langData: any;
  constructor(public authService: AuthService, public navCtrl: NavController, public navParams: NavParams) {
    this.langData = JSON.parse(localStorage.getItem('clang'));
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data.userData;
    this.catDetails.userid = this.userDetails.id;
    this.catDetails.token = this.userDetails.token;
    this.getCat();
  }

  getCat() {
    this.authService.postData(this.catDetails, "companycat").then((result) => {
      this.catData = result;
      if (this.catData.getCompCatList) {
        this.catList = this.catData.getCompCatList;
        console.log(this.catList);
      }
      else {
        console.log("problem categories");
      }
    }, (err) => {

    });
  }

  adSubCat(i){
    this.navCtrl.push(CompsubcatPage, {"cat":this.catList[i]})
    //.then(() => {
   //   let index = this.navCtrl.length()-2;
   //   this.navCtrl.remove(index);
  //  });
  }

}
