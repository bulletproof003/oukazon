import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ViewController, ModalController, ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Observable } from 'rxjs/Observable';
import { Common } from '../../providers/common';
import { Storage } from '@ionic/storage';
import { MapProvider } from '../../providers/map/map';
import { AuthService } from '../../providers/auth-service/auth-service';
import { SelectSearchable } from '../../components/select/select';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-edit-ask',
  templateUrl: 'edit-ask.html',
})
export class EditAskPage {
  my: string = "pp";
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('searchbar', { read: ElementRef }) searchbar: ElementRef;
  addressElement: HTMLInputElement = null;
  responseData: any;
  cityData: any;
  cityList: any;
  customData: any;
  customList: any;
  resposeData: any;
  TransData: any;
  adData: any;
  locationDatas = {
    city_id: "",
    city_name: "",
    state_id: "",
    state_name: "",
  };
  customDetails = { "userid": "", "token": "", "c_cat_id": "", "c_subcat_id": "" };
  locationData: any;
  locationData1: any;
  cityDetails = { "userid": "", "token": "" };
  negotiable = { value: false };
  hide_phone = { value: false };
  contact_phone = { value: true };
  contact_email = { value: true };
  contact_chat = { value: true };
  agree = { value: true };
  paypal = { value: false };
  oukapoints = { value: false };
  pay_paypal: any;
  pay_oukapoints: any;
  terms = {
    agrees: ""
  };
  map: any;
  address1: any;
  address2: any;
  address = '';
  latlongs: any;
  public base64Image: string;
  public image: any;
  infoWindow: any;
  askLocation: String;
  askLoc: any;
  lastImage: string = null;
  loading: Loading;
  cat: any;
  payments: any;
  resource: any;
  urgent_fee: any;
  USDQAR: any;
  UQ: any;
  amount: any;
  user_oukapoints: any;
  urgent_fee_oukapoint: any;
  public custom_text: any;
  public custom_textarea: any;
  public custom_select: any;
  public custom_checkbox: any;
  public custom_radio: any;
  public custom_title: any;
  public custom_types: any;
  public userDetails: any;
  constructor(private alertCtrl: AlertController,
    public common: Common,
    public viewCtrl: ViewController,
    public zone: NgZone,
    public localStore: Storage,
    public mapService: MapProvider,
    public modalCtrl: ModalController,
    public toaster: ToastController,
    public locac: LocationAccuracy,
    public geocoder: NativeGeocoder,
    public geolocation: Geolocation,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public platform: Platform,
    public authService: AuthService,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController) {
    this.common.presentLoading();
    this.adData = this.navParams.get("question");
    console.log(this.adData);
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data.userData;
    this.adData.user_id = this.userDetails.id;
    this.adData.token = this.userDetails.token;
    this.cityDetails.userid = this.userDetails.id;
    this.cityDetails.token = this.userDetails.token;
    this.agreeTerms();
    this.getCities();
    if (this.adData.hide_phone == "1") {this.hide_phone.value = true} else {this.hide_phone.value = false}
    if (this.adData.contact_phone == "1") {this.contact_phone.value = true} else {this.contact_phone.value = false}
    if (this.adData.contact_chat == "1") {this.contact_chat.value = true} else {this.contact_chat.value = false}
    if (this.adData.contact_email == "1") {this.contact_email.value = true} else {this.contact_email.value = false}
    this.common.closeLoading();
  }
  getCities() {
    this.authService.postData(this.cityDetails, "getCity").then((result) => {
      this.cityData = result;
      if (this.cityData.getCityList) {
        this.cityList = this.cityData.getCityList;
        console.log(this.cityList);
      }
      else {
        console.log("problem cities");
      }
      for (let i = 0; i < this.cityList.length; i++) {
        if (this.cityList[i].city_id == this.adData.city) {
          this.locationDatas.city_id = this.cityList[i].city_id;
          this.locationDatas.city_name = this.cityList[i].city_name;
          this.locationDatas.state_id = this.cityList[i].state_id;
          this.locationDatas.state_name = this.cityList[i].state_name;
        }
      }
      this.locationData = this.locationDatas;
      console.log(this.locationDatas);
    }, (err) => {

    });
  }
 
  portChange(event: { component: SelectSearchable, value: any }) {
    console.log('value:', event.value);
  }

  h_phone() {
    if (this.hide_phone.value == false) { this.adData.hide_phone = "0"; } else { this.adData.hide_phone = "1"; }
  }
  phone() {
    if (this.contact_phone.value == true) { this.adData.contact_phone = "1"; } else { this.adData.contact_phone = "0"; }
  }
  email() {
    if (this.contact_email.value == true) { this.adData.contact_email = "1"; } else { this.adData.contact_email = "0"; }
  }
  chat() {
    if (this.contact_chat.value == true) { this.adData.contact_chat = "1"; } else { this.adData.contact_chat = "0"; }
  }
  agreeTerms() {
    if (this.agree.value == true) { this.terms.agrees = "1"; } else { this.terms.agrees = "0"; }
  }

  ngOnInit() {
    this.locationData = [];
    this.platform.ready().then(() => this.loadMaps());
    this.resource = [];
  }

  loadMaps() {
    if (!!google) {
      this.initializeMap();
      this.initAutocomplete();
    } else {
      this.errorAlert('Error', 'Something went wrong with the Internet Connection. Please check your Internet.')
    }
  }

  initializeMap() {
    let that = this;
    var lat; var lng;
    //that.currentLocation();
    lat = parseFloat(this.adData.lat);
    lng = parseFloat(this.adData.long);
    this.zone.run(() => {
      var mapEle = this.mapElement.nativeElement;
      this.map = new google.maps.Map(mapEle, {
        zoom: 14,
        center: { 
          lat: lat,
          lng: lng
         }, 
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }],
        disableDoubleClickZoom: false,
        disableDefaultUI: true,
        zoomControl: true,
        scaleControl: true,
        componentRestrictions: {
          country: 'QA',
        }
      });
        let latLngObj = { 'lat': lat, 'long': lng };
        that.getAddress(latLngObj);


      // Map drag started
      this.map.addListener('dragstart', function () {
        console.log('Drag start');
      });
      // Map dragging
      
        this.map.addListener('drag', function () {
          that.address = 'Searching...';
        });
      
        
      //Reload markers every time the map moves
      this.map.addListener('dragend', function () {
        
        let map_center = that.getMapCenter();
        let latLngObj = { 'lat': map_center.lat(), 'long': map_center.lng() };
        console.log(latLngObj);
        that.getAddress(latLngObj);
      });

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        google.maps.event.trigger(this.map, 'resize');
        mapEle.classList.add('show-map');
      });

      google.maps.event.addListener(this.map, 'bounds_changed', () => {
        this.zone.run(() => {
          that.resizeMap();
        });
      });


    });
  }

  initAutocomplete(): void {
    this.addressElement = this.searchbar.nativeElement.querySelector('.searchbar-input');
    this.createAutocomplete(this.addressElement).subscribe((location) => {
      console.log('Searchdata', location);
      let latLngObj = { 'lat': location.lat(), 'long': location.lng() };
      this.adData.latlong = location.lat() + "," + location.lng();
      console.log(this.adData.latlong);
      this.getAddress(latLngObj);
      console.log(latLngObj);
      console.log("22222222222");
      let options = {
        center: location,
        zoom: 14,
        componentRestrictions: { country: 'QA' }
      };
      this.map.setOptions(options);
    });
  }

  currentLocation() {

    this.geolocation.getCurrentPosition().then((position) => {
      let latLngObj = { 'lat': position.coords.latitude, 'long': position.coords.longitude };
      this.latlongs = position.coords.latitude + "," + position.coords.longitude;
      console.log(this.latlongs);
      this.adData.latlong = this.latlongs;
      // Display  Marker
      this.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
      this.getAddress(latLngObj);

      localStorage.setItem('current_latlong', JSON.stringify(latLngObj));
      console.log(latLngObj);
      console.log("3333333333");
      return latLngObj;


    }, (err) => {
      console.log(err);
    });
  }

  getAddress(latLngObj) {
    // Get the address object based on latLngObj
    this.mapService.getStreetAddress(latLngObj).subscribe(
      s_address => {
        this.address2 = s_address.results;
        if (s_address.status == "ZERO_RESULTS") {
          this.mapService.getAddress(latLngObj).subscribe(
            results => {
              this.address1 = results.results;
              this.address = this.address1[0].formatted_address;
                this.getAddressComponentByPlace(this.address1[0], latLngObj);
              this.adData.location = this.address;
              console.log(this.adData.location);
            },
            err => console.log("Error in getting the street address " + err)
          )
        } else {
          this.address = this.address2[0].formatted_address;
            this.getAddressComponentByPlace(this.address2[0], latLngObj);
          this.adData.location = this.address;
          console.log(this.address);
        }
      },
      err => {
        console.log('No Address found ' + err);
      }
    );

  }

  getMapCenter() {
    return this.map.getCenter()
  }

  createAutocomplete(addressEl: HTMLInputElement): Observable<any> {
    const autocomplete = new google.maps.places.Autocomplete(addressEl);
    autocomplete.bindTo('bounds', this.map);
    return new Observable((sub: any) => {
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        const place = autocomplete.getPlace();
        if (!place.geometry) {
          sub.error({
            message: 'Autocomplete returned place with no geometry'
          });
        } else {
          let latLngObj = { 'lat': place.geometry.location.lat(), 'long': place.geometry.location.lng() }
          this.getAddress(latLngObj);
          sub.next(place.geometry.location);
        }
      });
    });
  }

  getAddressComponentByPlace(place, latLngObj) {
    var components;

    components = {};

    for (var i = 0; i < place.address_components.length; i++) {
      let ac = place.address_components[i];
      components[ac.types[0]] = ac.long_name;
    }
    let addressObj = {

      street: (components.street_number) ? components.street_number : 'not found',
      area: components.route,
      city: (components.sublocality_level_1) ? components.sublocality_level_1 : components.locality,
      country: (components.administrative_area_level_1) ? components.administrative_area_level_1 : components.political,
      postCode: components.postal_code,
      loc: [latLngObj.long, latLngObj.lat],
      address: this.address
    }
    localStorage.setItem('carryr_customer', JSON.stringify(addressObj));
    return components;
  }

  resizeMap() {
    setTimeout(() => {
      google.maps.event.trigger(this.map, 'resize');
    }, 200);
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  errorAlert(title, message) {
    alert('Error in Alert');
  }

  makeid() {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  };

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  tagAlert() {
    let alert = this.alertCtrl.create({
      title: 'TAG Field',
      subTitle: 'TAG Field is required !',
      buttons: ['OK']
    });
    alert.present();
  }

  cityAlert() {
    let alert = this.alertCtrl.create({
      title: 'CITY Field',
      subTitle: 'CITY Field is required ! Please select a City',
      buttons: ['OK']
    });
    alert.present();
  }

  agreeAlert() {
    let alert = this.alertCtrl.create({
      title: 'Agree to Terms',
      subTitle: 'Please, Check the AGREE checkBox If You agree to the terms and conditions. !',
      buttons: ['OK']
    });
    alert.present();
  }

  Done() {
    let alert = this.alertCtrl.create({
      title: 'Congratulation',
      subTitle: 'Your Question Have been Edited Successfully !',
      buttons: ['Done']
    });
    alert.present();
  }

  postAdFree(){
    this.authService.postData(this.adData, "editAsk").then((result) => {
      this.resposeData = result;
      if (this.resposeData.success) {
      this.presentToast('Succesfully Edited.');
    } else {
      this.presentToast('Problem While Editing Question.');
    }
    }, (error) => {
      this.presentToast(error);
    });
  }

  postAsk() {
    console.log(this.terms.agrees);
    this.adData.city = this.locationData.city_id;
    this.adData.state = this.locationData.state_id;
    this.adData.latlong = this.adData.latlong.toString();
    this.adData.screen_shot = this.adData.screen_shot.toString();
    this.adData.tag = this.adData.tag.toString();
    console.log(this.adData.city);
    if (this.adData.tag === '') {
      this.tagAlert();
    } else if (this.adData.city === undefined) {
      this.cityAlert();
    } else if (this.terms.agrees === '0') {
      this.agreeAlert();
    } else {

        this.postAdFree();
      
      setTimeout(() => {
        this.navCtrl.pop();
        this.Done();
      }, 3000);
    }
  }



}