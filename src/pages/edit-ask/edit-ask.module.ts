import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAskPage } from './edit-ask';

@NgModule({
  declarations: [
    EditAskPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAskPage),
  ],
})
export class EditAdPageModule {}
