import { Component } from '@angular/core';
import { IonicPage, ViewController, ToastController } from 'ionic-angular';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-map-autocomplete',
  templateUrl: 'map-autocomplete.html',
})
export class MapAutocompletePage {
  autocompleteItems: any;
  autocomplete: any;
  acService: any;
  placesService: any;

  constructor(public toaster: ToastController, public viewCtrl: ViewController) {
  }
  public presentToast(text) {
    let toast = this.toaster.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  ngOnInit() {
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {
    console.log('modal > chooseItem > item > ', item);
    this.viewCtrl.dismiss(item);
  }

  updateSearch() {
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      types: ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: this.autocomplete.query,
      componentRestrictions: { country: 'QA' }
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      console.log('modal > getPlacePredictions > predictions > ', predictions);
      if (predictions === null){
        self.presentToast('No Result, Please Try Another Address');
      } else {
      self.autocompleteItems = [];
      predictions.forEach(function (prediction) {
        self.autocompleteItems.push(prediction);
      }, error => {
        alert('No Result Found ! .');
      });
    }}, error => {
      alert('No Result Found ! .');
    });
    error => {
      alert('No Result Found ! .');
    }
  }

}
