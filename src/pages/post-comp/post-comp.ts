import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ViewController, ModalController, ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { MapProvider } from '../../providers/map/map';
import { AuthService } from '../../providers/auth-service/auth-service';
import { ProductService } from '../../providers/product-service';
import { SelectSearchable } from '../../components/select/select';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
declare var cordova: any;
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-post-comp',
  templateUrl: 'post-comp.html',
})
export class PostCompPage {
  myy: string = "ppp";
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('searchbar', { read: ElementRef }) searchbar: ElementRef;
  addressElement: HTMLInputElement = null;
  responseData: any;
  cityData: any;
  cityList: any;
  customData: any;
  customList: any;
  resposeData: any;
  TransData: any;
  compData = {
    token: "",
    urgent: "",
    user_id: "",
    category: "",
    sub_category: "",
    company_name: "",
    company_namear: "",
    description: "",
    descriptionar: "",
    phone: "",
    hide_phone: "",
    location: "",
    city: "",
    state: "",
    country: "178",
    latlong: "",
    screen_shot: "",
    tag: "",
    landline: "",
    fax: "",
    email: "",
    web: "",
    status: "active",
    contact_phone: "",
    contact_email: "",
    contact_chat: "",
    custom_fields: "",
    custom_types: "",
    custom_values: "",
  };
  locationData = {
    cc: ""
  };
  transactionData = {
    token: "",
    company_name: "",
    company_id: "",
    seller_id: "",
    amount: "",
    amount1: "",
    urgent: "1",
    status: "",
    transaction_gatway: "",
    transaction_ip: "::1",
    transaction_description: "Directory Fee",
    transaction_method: "Pay Directory Fee",
  };
  locationData1: any;
  cityDetails = { "userid": "", "token": "" };
  customDetails = { "userid": "", "token": "", "c_cat_id": "", "c_subcat_id": "" };
  hide_phone = { value: false };
  contact_phone = { value: true };
  contact_email = { value: true };
  contact_chat = { value: true };
  agree = { value: true };
  comppaypal = { value: false };
  compoukapoints = { value: false };
  pay_paypal: any;
  pay_oukapoints: any;
  disable: boolean;
  terms = {
    agrees: ""
  };
  map: any;
  address = '';
  latlongs: any;
  public photos: any;
  public base64Image: string;
  public image: any;
  infoWindow: any;
  askLocation: String;
  askLoc: any;
  lastImage: string = null;
  loading: Loading;
  cat: any;
  payments: any;
  resource: any;
  dir_fee: any;
  USDQAR: any;
  UQ: any;
  amount: any;
  user_oukapoints: any;
  dir_fee_oukapoint: any;
  public custom_text: any;
  public custom_textarea: any;
  public custom_select: any;
  public custom_checkbox: any;
  public custom_radio: any;
  public custom_title: any;
  public custom_types: any;
  public userDetails: any;
  constructor(private alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public zone: NgZone,
    public localStore: Storage,
    public mapService: MapProvider,
    public modalCtrl: ModalController,
    public toaster: ToastController,
    public locac: LocationAccuracy,
    public geocoder: NativeGeocoder,
    public geolocation: Geolocation,
    public navParams: NavParams,
    private transfer: Transfer,
    private file: File,
    private filePath: FilePath,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public platform: Platform,
    public authService: AuthService,
    private productService: ProductService,
    public navCtrl: NavController,
    private camera: Camera,
    private payPal: PayPal,
    public loadingCtrl: LoadingController) {
    this.cat = this.navParams.get("adsubcat");
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data.userData;
    this.compData.user_id = this.userDetails.id;
    this.compData.token = this.userDetails.token;
    this.compData.category = this.cat.main_cat_id;
    this.compData.sub_category = this.cat.sub_cat_id;
    this.cityDetails.userid = this.userDetails.id;
    this.cityDetails.token = this.userDetails.token;
    this.transactionData.token = this.userDetails.token;
    this.user_oukapoints = this.userDetails.oukapoints;
    this.compData.category = this.cat.main_cat_id;
    this.compData.sub_category = this.cat.sub_cat_id;
    this.customDetails.c_cat_id = this.cat.main_cat_id;
    this.customDetails.c_subcat_id = this.cat.sub_cat_id;
    this.customDetails.userid = this.userDetails.id;
    this.customDetails.token = this.userDetails.token;
    this.getPayment();
    this.h_phone();
    this.phone();
    this.email();
    this.chat();
    this.agreeTerms();
    this.getCities();
    this.getCustoms();
    this.pp_paypal();
    this.pp_oukapoints();
    this.getRate();
  }
  getCities() {
    this.authService.postData(this.cityDetails, "getCity").then((result) => {
      this.cityData = result;
      if (this.cityData.getCityList) {
        this.cityList = this.cityData.getCityList;
        console.log(this.cityList);
      }
      else {
        console.log("problem cities");
      }
    }, (err) => {
    });
  }
  private getCustoms() {
    this.authService.postData(this.customDetails, "getCustomDir").then((result) => {
      this.customData = result;
      if (this.customData.getCustomList) {
        this.customList = this.customData.getCustomList;
        var i;
        for (i = 0; i < this.customList.length; i++) {
          this.customList[i].custom_options = this.customList[i].custom_options.split(",");
          if (this.custom_title.length < this.customList.length) {
            this.custom_title.push(this.customList[i].custom_title);
          }
          if (this.custom_types.length < this.customList.length) {
            this.custom_types.push(this.customList[i].custom_type);
          }
        }
        this.compData.custom_fields = this.custom_title.join(',');
        this.compData.custom_types = this.custom_types.join(',');
        console.log(this.customList);
        console.log(this.compData.custom_fields);
        console.log(this.compData.custom_types);
      }
      else {
        console.log("problem Custom");
      }
    }, (err) => {

    });
  }
  portChange(event: { component: SelectSearchable, value: any }) {
    console.log('value:', event.value);
  }

  pp_paypal() {
    if (this.comppaypal.value == false) { this.pay_paypal = "0"; } else { this.pay_paypal = "1"; this.compoukapoints.value = false }
  }
  pp_oukapoints() {
    if (this.compoukapoints.value == false) { this.pay_oukapoints = "0"; } else { this.pay_oukapoints = "1"; this.comppaypal.value = false }
  }
  h_phone() {
    if (this.hide_phone.value == false) { this.compData.hide_phone = "0"; } else { this.compData.hide_phone = "1"; }
  }
  phone() {
    if (this.contact_phone.value == true) { this.compData.contact_phone = "1"; } else { this.compData.contact_phone = "0"; }
  }
  email() {
    if (this.contact_email.value == true) { this.compData.contact_email = "1"; } else { this.compData.contact_email = "0"; }
  }
  chat() {
    if (this.contact_chat.value == true) { this.compData.contact_chat = "1"; } else { this.compData.contact_chat = "0"; }
  }
  agreeTerms() {
    if (this.agree.value == true) { this.terms.agrees = "1"; } else { this.terms.agrees = "0"; }
  }

  ngOnInit() {
    //this.platform.ready().then(() => this.loadMaps());
    this.requestAccuracy();
    this.photos = [];
    this.custom_text = [];
    this.custom_textarea = [];
    this.custom_select = [];
    this.custom_checkbox = [];
    this.custom_radio = [];
    this.custom_title = [];
    this.custom_types = [];
    this.resource = [];
  }

  requestAccuracy() {
    this.locac.canRequest().then((canRequest: boolean) => {
      if(canRequest) {
        this.locac.request(this.locac.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
            console.log('Request successful.');
            this.platform.ready().then(() => this.loadMaps());
          },
          error => {
            console.log('Error requesting location permissions', error)
            this.closeModal();
          }
        );
      }
    });
  }

  getPayment() {
    this.productService.getPayments().subscribe(
      datax => {
        this.payments = datax.payment;
        console.log(this.payments);
        this.dir_fee = this.payments[0].dir_fee;
        console.log(this.dir_fee);
        this.dir_fee_oukapoint = this.payments[0].dir_fee_oukapoint;
        if (parseInt(this.user_oukapoints) > parseInt(this.dir_fee_oukapoint)) {
          this.disable = true;
        } else {
          this.disable = false;
        }
        console.log(this.disable);
        console.log(this.dir_fee_oukapoint);
        console.log(this.user_oukapoints);
    },
      error =>{
        console.log("error Get Payment");
    });
  }

  getRate() {
    this.productService.getCurrencies().then((result) => {
      this.resource.push(result);
      this.USDQAR = this.resource[0].quotes;
      this.UQ = this.USDQAR.USDQAR;
      this.amount = this.dir_fee / this.UQ;
      this.amount = this.amount.toFixed(2);
      console.log(this.USDQAR.USDQAR);
      console.log(this.USDQAR);
    },error =>{
        console.log("error Get Payment");
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
  }

  loadMaps() {
    if (!!google) {
      this.initializeMap();
      this.initAutocomplete();
    } else {
      this.errorAlert('Error', 'Something went wrong with the Internet Connection. Please check your Internet.')
    }
  }

  initializeMap() {
    let that = this;
    that.currentLocation();
    this.zone.run(() => {
      var mapEle = this.mapElement.nativeElement;
      this.map = new google.maps.Map(mapEle, {
        zoom: 14,
        center: { lat: 25.2854473, lng: 51.53104 },
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        //styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }],
        disableDoubleClickZoom: false,
        disableDefaultUI: true,
        zoomControl: true,
        scaleControl: true,
        componentRestrictions: {
          country: 'QA',
        }
      });


      // Map drag started
      this.map.addListener('dragstart', function () {
        console.log('Drag start');
      });
      // Map dragging
      this.map.addListener('drag', function () {
        that.address = 'Searching...';
      });
      //Reload markers every time the map moves
      this.map.addListener('dragend', function () {
        let map_center = that.getMapCenter();
        let latLngObj = { 'lat': map_center.lat(), 'long': map_center.lng() };
        console.log(latLngObj);
        that.getAddress(latLngObj);
      });

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        google.maps.event.trigger(this.map, 'resize');
        mapEle.classList.add('show-map');
      });

      google.maps.event.addListener(this.map, 'bounds_changed', () => {
        this.zone.run(() => {
          this.resizeMap();
        });
      });


    });
  }

  initAutocomplete(): void {
    this.addressElement = this.searchbar.nativeElement.querySelector('.searchbar-input');
    this.createAutocomplete(this.addressElement).subscribe((location) => {
      console.log('Searchdata', location);
      let latLngObj = { 'lat': location.lat(), 'long': location.lng() };
      this.compData.latlong = location.lat() + "," + location.lng();
      console.log(this.compData.latlong);
      this.getAddress(latLngObj);
      console.log(latLngObj);
      console.log("22222222222");
      let options = {
        center: location,
        zoom: 14,
        componentRestrictions: { country: 'QA' }
      };
      this.map.setOptions(options);
    });
  }

  currentLocation() {

    this.geolocation.getCurrentPosition().then((position) => {
      //let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let latLngObj = { 'lat': position.coords.latitude, 'long': position.coords.longitude };
      this.latlongs = position.coords.latitude + "," + position.coords.longitude;
      console.log(this.latlongs);
      this.compData.latlong = this.latlongs;
      // Display  Marker
      this.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
      this.getAddress(latLngObj);

      localStorage.setItem('current_latlong', JSON.stringify(latLngObj));
      console.log(latLngObj);
      console.log("3333333333");
      return latLngObj;


    }, (err) => {
      console.log(err);
    });
  }

  getAddress(latLngObj) {
    // Get the address object based on latLngObj
    this.mapService.getStreetAddress(latLngObj).subscribe(
      s_address => {
        if (s_address.status == "ZERO_RESULTS") {
          this.mapService.getAddress(latLngObj).subscribe(
            address => {
              this.address = address.results[0].formatted_address;
              this.compData.location = this.address;
              this.getAddressComponentByPlace(address.results[0], latLngObj);
            },
            err => console.log("Error in getting the street address " + err)
          )
        } else {
          this.address = s_address.results[0].formatted_address;
          this.compData.location = this.address;
          this.getAddressComponentByPlace(s_address.results[0], latLngObj);
          console.log(this.address);
        }
      },
      err => {
        console.log('No Address found ' + err);
      }
    );

  }

  getMapCenter() {
    return this.map.getCenter()
  }

  createAutocomplete(addressEl: HTMLInputElement): Observable<any> {
    const autocomplete = new google.maps.places.Autocomplete(addressEl);
    autocomplete.bindTo('bounds', this.map);
    return new Observable((sub: any) => {
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        const place = autocomplete.getPlace();
        if (!place.geometry) {
          sub.error({
            message: 'Autocomplete returned place with no geometry'
          });
        } else {
          let latLngObj = { 'lat': place.geometry.location.lat(), 'long': place.geometry.location.lng() }
          this.getAddress(latLngObj);
          sub.next(place.geometry.location);
        }
      });
    });
  }

  getAddressComponentByPlace(place, latLngObj) {
    var components;

    components = {};

    for (var i = 0; i < place.address_components.length; i++) {
      let ac = place.address_components[i];
      components[ac.types[0]] = ac.long_name;
    }
    let addressObj = {

      street: (components.street_number) ? components.street_number : 'not found',
      area: components.route,
      city: (components.sublocality_level_1) ? components.sublocality_level_1 : components.locality,
      country: (components.administrative_area_level_1) ? components.administrative_area_level_1 : components.political,
      postCode: components.postal_code,
      loc: [latLngObj.long, latLngObj.lat],
      address: this.address
    }
    localStorage.setItem('carryr_customer', JSON.stringify(addressObj));
    return components;
  }

  resizeMap() {
    setTimeout(() => {
      google.maps.event.trigger(this.map, 'resize');
    }, 200);
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  errorAlert(title, message) {
    alert('Error in Alert');
  }

  makeid() {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  };

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 60,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      targetWidth: 1000,
      targetHeight: 1000,
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      if (this.photos.length < 5) {
        this.photos.push(this.lastImage);
      } else {
        this.presentToast('Maximum 5 Pictures allowed.');
      }
      this.photos.reverse();
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(imgs) {
    if (imgs === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + imgs;
    }
  }

  public uploadImage() {
    // Destination URL
    var url = "https://www.oukazon.com/app_upload/comp_uploads.php";
    const fileTransfer: TransferObject = this.transfer.create();
    var i;
    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',});
    this.loading.present();

    for (i = 0; i < this.photos.length; i++) {
      // File for Upload
      var targetPath = this.pathForImage(this.photos[i]);

      // File name only
      var filename = this.photos[i];
      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params: { 'fileName': filename }
      };

      // Use the FileTransfer to upload the image
      fileTransfer.upload(targetPath, url, options).then(data => {
        this.loading.dismissAll()
      }, err => {
        this.loading.dismissAll()
        this.presentToast('Error while uploading file.');
      });
    }
    this.presentToast('All Images succesfully uploaded.');
  }

  deletePhoto(index) {
    let confirm = this.alertCtrl.create({
      title: 'Sure you want to delete this photo? There is NO undo!',
      message: '',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Agree clicked');
            this.photos.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  tagAlert() {
    let alert = this.alertCtrl.create({
      title: 'TAG Field',
      subTitle: 'TAG Field is required !',
      buttons: ['OK']
    });
    alert.present();
  }

  cityAlert() {
    let alert = this.alertCtrl.create({
      title: 'CITY Field',
      subTitle: 'CITY Field is required ! Please select a City',
      buttons: ['OK']
    });
    alert.present();
  }

  custom_F_Alert() {
    let alert = this.alertCtrl.create({
      title: 'Additional Info (!)',
      subTitle: 'Additional Info is required ! Please fill the required infos !',
      buttons: ['OK']
    });
    alert.present();
  }
  agreeAlert() {
    let alert = this.alertCtrl.create({
      title: 'Agree to Terms',
      subTitle: 'Please, Check the AGREE checkBox If You agree to the terms and conditions. !',
      buttons: ['OK']
    });
    alert.present();
  }

  adPhotos() {
    let alert = this.alertCtrl.create({
      title: 'Ad PHOTOS (?)',
      subTitle: 'Please, upload Photos of your Company , Max 5 Photos. !',
      buttons: ['OK']
    });
    alert.present();
  }

  Done() {
    let alert = this.alertCtrl.create({
      title: 'Congratulation',
      subTitle: 'Your Company Have been Listed Successfully !',
      buttons: ['Done']
    });
    alert.present();
  }

  private xxxx() {
    if (this.custom_select.length != this.custom_types.length) {
      this.custom_F_Alert();
      this.custom_select = [];
    } else if (this.custom_select.length === this.custom_types.length) {
      this.custom_text = [];
      var i;
      for (i = 0; i < this.custom_select.length; i++) {
        if (Array.isArray(this.custom_select[i])) {
          this.custom_text.push(this.custom_select[i].join('+'));
          console.log(this.custom_select[i]);
        } else {
          this.custom_text.push(this.custom_select[i]);
        }
      }
      this.compData.custom_values = this.custom_text.join(',');
    }
  }

  use_paypal(){
    this.payPal.init({
      PayPalEnvironmentProduction: 'AQYIr49PhOkqKMWgNa4rxyZJyqcRvxsKyxLqGBx-CHk8gw_-pCU204-uD-6OwowHXD9QbfbyNtRvZOno',
      PayPalEnvironmentSandbox: 'ASLFtPEEiehIE2H-vh50AoRKgJ4RXYezr3are84yiit6lUpMav7WMDBf9_u-Tz2mjpQ0BZDilp6XGLqP'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentProduction', new PayPalConfiguration({
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment(this.amount, 'USD', "Pay Directory Fee : " + this.compData.company_name, 'sale');
        this.payPal.renderSinglePaymentUI(payment).then(() => {
          // Successfully paid
    
          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, (err) => {
          console.log('Error or render dialog closed without being successful');
        });
      }, (err) => {
        console.log('Error in configuration');
      });
    }, (err) => {
      console.log('Error in initialization, maybe PayPal is not supported or something else');
    });
  }

  postAdurgent(){
    this.authService.postData(this.compData, "postComp").then((result) => {
      this.resposeData = result;
      this.TransData = this.resposeData.compTransactionData;
      this.transactionData.company_id = this.TransData.id;
      this.transactionData.company_name = this.TransData.company_name;
      this.transactionData.seller_id = this.TransData.user_id;
      this.authService.postData(this.transactionData, "postCompTransaction").then((result) => {
        this.responseData = result;
        if(this.responseData.userData){
        localStorage.setItem('userData', JSON.stringify(this.responseData) );
      }}, (err) => {
        this.presentToast('Problem with transaction.');
      });
      this.presentToast('Company & Transaction Succesfully Posted.');
    }, (err) => {
      this.presentToast('Problem While Posting Data Company Urgent.');
    });
  }

  NoDone() {
    let alert = this.alertCtrl.create({
      title: 'Directory Fee!',
      subTitle: 'Please, Make Sure to pay Directory Fee, either with paypal Or Oukapoints !',
      buttons: ['Done']
    });
    alert.present();
  }

  titleAlert() {
    let alert = this.alertCtrl.create({
      title: 'Title Field',
      subTitle: 'Please, Give Your Company a title, (at least one Language required) !',
      buttons: ['OK']
    });
    alert.present();
  }

  descAlert() {
    let alert = this.alertCtrl.create({
      title: 'Description Field',
      subTitle: 'Please, Give Your Company a Description, (at least one Language required) !',
      buttons: ['OK']
    });
    alert.present();
  }

  PicAlert() {
    let alert = this.alertCtrl.create({
      title: 'Photos Field',
      subTitle: 'Please, Upload at Least One Photos, (Max 5 Photos) !',
      buttons: ['OK']
    });
    alert.present();
  }

  postAsk() {
    console.log(this.terms.agrees);
    this.locationData1 = this.locationData.cc;
    this.compData.city = this.locationData1.city_id;
    this.compData.state = this.locationData1.state_id;
    console.log(this.compData.city);
    if (this.compData.company_namear === '' && this.compData.company_name === '') {
      this.titleAlert();
    } else if (this.compData.descriptionar === '' && this.compData.description === '') {
      this.descAlert();
    } else if (this.custom_select.length != this.custom_types.length) {
      this.xxxx();
    } else if (this.photos.length === 0) {
      this.PicAlert();
    } else if (this.compData.city === undefined) {
      this.cityAlert();
    } else if (this.compData.landline === '') {this.compData.landline = ''}
    else if (this.compData.fax === '') {this.compData.fax = ''}
    else if (this.compData.email === '') {this.compData.email = ''}
    else if (this.compData.web === '') {this.compData.web = ''}
    else if (this.terms.agrees === '0') {
      this.agreeAlert();
    } else {
      

      if (this.comppaypal.value === true) {
        this.xxxx();
        if (this.photos.length > 0) {
          this.uploadImage();
        } else {
          this.adPhotos();
          //console.log('Photos was here');
        }
        this.transactionData.transaction_gatway = 'paypal';
        this.transactionData.amount = this.dir_fee;
        this.transactionData.amount1 = this.dir_fee;
        this.transactionData.status = 'pending';
        this.compData.status = "active";
        this.compData.urgent = "0";
        this.use_paypal();
        this.postAdurgent();
      } else if (this.compoukapoints.value === true) {
        this.xxxx();
        if (this.photos.length > 0) {
          this.uploadImage();
        } else {
          this.adPhotos();
          //console.log('Photos was here');
        }
        this.transactionData.transaction_gatway = 'oukapoint';
        this.transactionData.amount = this.dir_fee_oukapoint;
        this.transactionData.amount1 = JSON.stringify(this.user_oukapoints - this.dir_fee_oukapoint);
        this.transactionData.status = 'success';
        this.compData.status = "active";
        this.compData.urgent = "1";
        this.postAdurgent();
      } else {
        this.NoDone();
      }
      
      setTimeout(() => {
        this.navCtrl.pop();
        this.Done();
      }, 3000);
    }
  }



}