import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostCompPage } from './post-comp';

@NgModule({
  declarations: [
    PostCompPage,
  ],
  imports: [
    IonicPageModule.forChild(PostCompPage),
  ],
})
export class PostCompPageModule {}
