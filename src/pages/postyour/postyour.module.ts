import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostyourPage } from './postyour';

@NgModule({
  declarations: [
    PostyourPage,
  ],
  imports: [
    IonicPageModule.forChild(PostyourPage),
  ],
})
export class PostyourPageModule {}
