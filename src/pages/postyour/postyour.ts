import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController } from 'ionic-angular';
import { AdcatPage } from '../adcat/adcat';
import { CompcatPage } from '../compcat/compcat';
import { PostAskPage } from '../post-ask/post-ask';

@IonicPage()
@Component({
  selector: 'page-postyour',
  template: `
  <ion-list style="margin: 0px 0 0px;">
    <ion-item (click)="postad()"><ion-icon name="cart"></ion-icon> Post Your Ad</ion-item>
    <ion-item (click)="postcomp()"><ion-icon name="folder"></ion-icon> List Your Company</ion-item>
    <ion-item (click)="postask()"><ion-icon name="help-circle"></ion-icon> Ask Your Question</ion-item>
  </ion-list>
`
})
export class PostyourPage {
constructor(public navCtrl: NavController, public viewCtrl: ViewController) {}

  close() {
    this.viewCtrl.dismiss();
  }
  postad(){
    this.navCtrl.push(AdcatPage);
    //this.close();
  }
  postcomp(){
    this.navCtrl.push(CompcatPage);
    //this.close();
  }
  postask(){
    this.navCtrl.push(PostAskPage);
    //this.close();
    
  }
}

