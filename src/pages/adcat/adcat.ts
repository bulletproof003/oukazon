import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { AdsubcatPage } from '../adsubcat/adsubcat';

@Component({
  selector: 'page-adcat',
  templateUrl: 'adcat.html',
})
export class AdcatPage {
  catDetails = { "userid": "", "token": "" };
  catData: any;
  catList: any;
  langData: any;
  userDetails: any;
  constructor(public authService: AuthService, public navCtrl: NavController, public navParams: NavParams) {
    this.langData = JSON.parse(localStorage.getItem('clang'));
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data.userData;
    this.catDetails.userid = this.userDetails.id;
    this.catDetails.token = this.userDetails.token;
    this.getCat();
  }

  getCat() {
    this.authService.postData(this.catDetails, "productcat").then((result) => {
      this.catData = result;
      if (this.catData.getAdCatList) {
        this.catList = this.catData.getAdCatList;
        console.log(this.catList);
      }
      else {
        console.log("problem categories");
      }
    }, (err) => {

    });
  }

  adSubCat(i){
    this.navCtrl.push(AdsubcatPage, {"cat":this.catList[i]});
    //.then(() => {
     // let index = this.navCtrl.length()-2;
     // this.navCtrl.remove(index);
  //  });
  }

}
