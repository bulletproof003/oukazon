import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Common } from '../../providers/common';
import { AskDetailPage } from '../ask-detail/ask-detail';
import { DirectoryPage } from '../directory/directory';
import { ClassifiedPage } from '../classified/classified';

@Component({
  selector: 'page-ask',
  templateUrl: 'ask.html',
})
export class AskPage {
  questions: any;
  questionsData: any;
  noRecordsAsk: boolean;
  responseDataAsk: any;
  userDetails: any;
  langData: any;
  userPostDataAsk = { 
    id: "", 
    token: "",
    updated_at: "",
  };
  constructor(public common: Common, public authService: AuthService, public navCtrl: NavController) {
    this.langData = JSON.parse(localStorage.getItem('clang'));
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
    this.userPostDataAsk.id = this.userDetails.id;
    this.userPostDataAsk.token = this.userDetails.token;
    this.userPostDataAsk.updated_at = '';
  }

  ionViewWillEnter() {
    this.common.presentLoading();
    this.userPostDataAsk.updated_at = '';
    this.noRecordsAsk = false;
    this.getQuestion();
    this.responseDataAsk = [];
    this.questionsData = [];
    this.questions = [];
    this.common.closeLoading();
  }


  doRefresh(refresher) {
    this.userPostDataAsk.updated_at = '';
    this.noRecordsAsk = false;
    this.questions = [];
    this.questionsData = [];
    this.getQuestion();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }

  showAsk(i) {
    this.navCtrl.push(AskDetailPage, { "question": this.questions[i] });
  }

  pushC() {
    this.navCtrl.push(DirectoryPage);
  }

  pushA() {
    this.navCtrl.push(ClassifiedPage);
  }

  getQuestion() {
    this.authService.postData(this.userPostDataAsk, "questions").then((result) => {
      this.responseDataAsk = result;
      if (this.responseDataAsk.questionData0) {
        this.questions = this.responseDataAsk.questionData0;
        this.userPostDataAsk.updated_at = this.questions[this.questions.length - 1].updated_at;
        console.log("Updated At " + this.userPostDataAsk.updated_at);
        var i;
        for (i = 0; i < this.questions.length; i++) {
          this.questions[i].userpic = "https://www.oukazon.com/storage/profile/small_" + this.questions[i].image;
          this.questions[i].screen_shot = this.questions[i].screen_shot.split(",");
          this.questions[i].tag = this.questions[i].tag.split(",");
          this.questions[i].latlong = this.questions[i].latlong.split(",");
          this.questions[i].lat = this.questions[i].latlong[0];
          this.questions[i].long = this.questions[i].latlong[1];
          this.questions[i].pictures = this.questions[i].screen_shot;
          this.questions[i].screen_shot = "https://www.oukazon.com/storage/questions/screenshot/small_" + this.questions[i].screen_shot[0];
        }
        console.log(this.questions);
      } else {
        console.log("no");
      }
    }, (err) => {

    });
  }

  doInfiniteAsk(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.authService.postData(this.userPostDataAsk, "questions").then((result) => {
        this.responseDataAsk = result;
        if (this.responseDataAsk.questionData0.length) {
          this.questionsData = this.responseDataAsk.questionData0;
          this.userPostDataAsk.updated_at = this.questionsData[this.questionsData.length - 1].updated_at;
          var a;
          for (a = 0; a < this.questionsData.length; a++) {
            this.questionsData[a].userpic = "https://www.oukazon.com/storage/profile/small_" + this.questionsData[a].image;
            this.questionsData[a].screen_shot = this.questionsData[a].screen_shot.split(",");
            this.questionsData[a].tag = this.questionsData[a].tag.split(",");
            this.questionsData[a].latlong = this.questionsData[a].latlong.split(",");
            this.questionsData[a].lat = this.questionsData[a].latlong[0];
            this.questionsData[a].long = this.questionsData[a].latlong[1];
            this.questionsData[a].pictures = this.questionsData[a].screen_shot;
            this.questionsData[a].screen_shot = "https://www.oukazon.com/storage/questions/screenshot/small_" + this.questionsData[a].screen_shot[0];
          }

          for (let m = 0; m < this.questionsData.length; m++) {
            this.questions.push(this.questionsData[m]);
          }
          console.log(this.questions);
        } else {
          this.noRecordsAsk = true;
        }
      }, (err) => {
  
      });
      
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

}
