import { Component, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { TabsPage } from '../tabs/tabs';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { SettingsPage } from '../settings/settings';
import { MessagesPage } from '../messages/messages';
import { LoginPage } from '../login/login';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Events } from 'ionic-angular';

export interface PageInterface {
  title: string;
  pageName: any;
  component: any;
  tabComponent?: any;
  index?: number;
  badge?: number;
  icon: string;
}
 
@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  // Basic root for our content view
  rootPage = HomePage;
  data: any;
  nb: any;
  nbmsgs: any;
  nbmsgs1: any;
  userDetails : any;
  userPostData = {"id":"","token":""};
  responseData : any;
  refreshIntervalId2 : any;
 
  // Reference to the app's root nav
  @ViewChild(Nav) nav: Nav;

  pages: PageInterface[] = [
    { title: 'Home', pageName: HomePage, component: HomePage, icon: 'home' },
    { title: 'Directory', pageName: TabsPage, component: TabsPage, tabComponent: 'DirectoryPage', index: 0, icon: 'folder' },
    { title: 'Classified Ads', pageName: TabsPage, component: TabsPage, tabComponent: 'ClassifiedPage', index: 1, icon: 'cart' },
    { title: 'Ask OUKA', pageName: TabsPage, component: TabsPage, tabComponent: 'AskPage', index: 2, icon: 'help-circle' },
    { title: 'My Dashboard', pageName: ProfilePage, component: ProfilePage, icon: 'person' },
    { title: 'Account Settings', pageName: SettingsPage, component: SettingsPage, icon: 'settings' },
    { title: 'Messages', pageName: MessagesPage, 
     component: MessagesPage, 
     badge: 0, 
     icon: 'chatbubbles' },
  ];

  constructor(private alertCtrl: AlertController, public platform: Platform, private localNotifications: LocalNotifications, private events: Events, public navCtrl: NavController, public navParams: NavParams, public authService:AuthService) {
    this.data = '';
    this.userDetails = '';
    this.data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = this.data.userData;
    this.userPostData.id = this.userDetails.id;
    this.userPostData.token = this.userDetails.token;
    console.log(this.data.userData);
    this.check_Msgs();
    this.events.publish("nbmsgs", this.nbmsgs, 1);
    platform.ready().then((readySource) => {
      this.localNotifications.on('click', (notification, state) => {
        this.nav.setRoot(MessagesPage);
        
      })
    });

   }

   ionViewWillEnter() {
  
    this.refreshIntervalId2 = setInterval(() => {
      this.check_Msgs();
      this.notify();
      this.events.publish("nbmsgs", this.nbmsgs, 1);
  }, 60000)
    
  }

  notify() {
    var b; var a;
    //that.currentLocation();
    b = parseFloat(this.nbmsgs1);
    a = parseFloat(this.nbmsgs);
    if (b==a && a=='0') {
      this.nbmsgs1 = this.nbmsgs;
    } else {
      if (this.nbmsgs1!=undefined && this.nbmsgs!=undefined) {
      this.localNotifications.schedule({
        id: 1,
        title: 'Oukazon Messenger',
        text: 'You have '+this.nbmsgs+' New Messages',
        icon: "res://icon.png",
        smallIcon:"res://icon.png"
      });
    }
      this.nbmsgs1 = this.nbmsgs;
    }
    

  }

   check_Msgs() {
    this.authService.postData(this.userPostData, "check_msg").then((result) => {
      this.responseData = result;
      if (this.responseData.check_msgData) {
        this.nb = this.responseData.check_msgData;
        this.nbmsgs = this.nb.nbmsg;
        this.events.publish("nbmsgs", this.nbmsgs, 1);
        console.log(this.nbmsgs);
      }
    })
  }

  continue() {
    let alert = this.alertCtrl.create({
      title: "DONE",
      message: 'Thank you for using OUKAZON App. Please Login To Continue !',
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Login",
          handler: () => {
            this.navCtrl.push(LoginPage);
          }
        }
      ]
    });
    alert.present();
}

   


  openPage(page: PageInterface) {
    let params = {};
 
    // The index is equal to the order of our tabs inside tabs.ts
    if (page.index) {
      params = { tabIndex: page.index };
    }
 
    // The active child nav is our Tabs Navigation
    if (this.nav.getActiveChildNav() && page.index != undefined) {
      this.nav.getActiveChildNav().select(page.index);
    } else {
      // Tabs are not active, so reset the root page 
      // In this case: moving to or from SpecialPage
      this.nav.setRoot(page.pageName, params);
    }
  }
 
  isActive(page: PageInterface) {
    // Again the Tabs Navigation
    let childNav = this.nav.getActiveChildNav();
 
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }
 
    // Fallback needed when there is no active childnav (tabs not active)
    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return 'primary';
    }
    return;
  }
 

  
}