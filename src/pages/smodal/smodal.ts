import { Component, Input, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Common } from '../../providers/common';
import { AdDetailPage } from '../ad-detail/ad-detail';
import { CompDetailPage } from '../comp-detail/comp-detail';
import { AskDetailPage } from '../ask-detail/ask-detail';

@Component({
  selector: 'page-smodal',
  templateUrl: 'smodal.html',
})
export class SmodalPage {
  @ViewChild('ser') myInput ;
  @Input() data: any;
  @Input() events: any;
  

  sections: any = {
    first: 'all',
    second: 'ads',
    third: 'comps',
    forth: 'ques',
 };

 selectedSection: string = this.sections.first;

  searchTerm: any = "";
  char: any;
  allItems: any;
  allItemsdir: any;
  allItemsask: any;
  responseData: any;
  userDetails: any;

  products: any;
  productsData: any;
  companies: any;
  companiesData: any;
  questions: any;
  questionsData: any;
  noRecordsAsk: boolean;
  responseDataComp: any;
  responseDataAsk: any;

  userPostData = { 
    id: "", 
    token: "",
    updated_at: "",
    cat_id: "",
    sub_cat_id: "",
  };
  userPostDataComp = { 
    id: "", 
    token: "",
    updated_at: "",
  };
  userPostDataAsk = { 
    id: "", 
    token: "",
    updated_at: "",
  };

  constructor(public viewCtrl: ViewController, public common: Common, public authService: AuthService, public navCtrl: NavController, public navParams: NavParams) {
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
    this.userPostData.id = this.userDetails.id;
    this.userPostData.token = this.userDetails.token;
    this.userPostDataComp.id = this.userDetails.id;
    this.userPostDataComp.token = this.userDetails.token;
    this.userPostDataAsk.id = this.userDetails.id;
    this.userPostDataAsk.token = this.userDetails.token;
    this.getProduct();
    this.getCompany();
    this.getQuestion();
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.myInput.setFocus();
    },150);
  }

  ngOnInit() {
    this.selectedSection = this.sections.first;
    this.common.presentLoading();
    this.products = [];
    this.companies = [];
    this.questions = [];
    this.common.closeLoading();
    setTimeout(() => {
      this.myInput.setFocus();
    },150);
  }

  buttonClickHandler() {
    this.selectedSection = this.sections.first;
  }

  seg2() {
    this.selectedSection = this.sections.second;
  }

  seg3() {
    this.selectedSection = this.sections.third;
  }

  seg4() {
    this.selectedSection = this.sections.forth;
  }

  search(event: any): void {
      if (!this.allItems) {
        this.allItems = this.products;
      }
      if (!this.allItemsdir) {
        this.allItemsdir = this.companies;
      }
      if (!this.allItemsask) {
        this.allItemsask = this.questions;
      }

      var test = function() {
        var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;        
        if(isArabic.test(this.searchTerm)){
          console.log('is arabic');
        }else{
          console.log('not arabic');
       }
    }

    var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;
      if (isArabic.test(this.searchTerm)) {
        this.char = 1;
        this.products = this.allItems.filter((product) => {
          return product.product_namear.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || product.descriptionar.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 ;
        });
        this.companies = this.allItemsdir.filter((company) => {
          return company.company_namear.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || company.descriptionar.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 ;
        });
        this.questions = this.allItemsask.filter((question) => {
          return question.question_namear.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || question.descriptionar.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 ;
        });

      } else {
        this.char = 0;
        this.products = this.allItems.filter((product) => {
          return product.product_name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || product.description.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 ;
        });
        this.companies = this.allItemsdir.filter((company) => {
          return company.company_name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || company.description.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 ;
        });
        this.questions = this.allItemsask.filter((question) => {
          return question.question_name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || question.description.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 ;
        });

      }
  }

  navigateTo1(){
    this.navCtrl.push(SmodalPage, {param2: 'ads'});
  }

  showAd(i) {
    this.navCtrl.push(AdDetailPage, { "product": this.products[i] });
  }

  showComp(i) {
    this.navCtrl.push(CompDetailPage, { "company": this.companies[i] });
  }

  showAsk(i) {
    this.navCtrl.push(AskDetailPage, { "question": this.questions[i] });
  }

  getProduct() {
    this.authService.postData(this.userPostData, "findproduct").then((result) => {
      this.responseData = result;
      if (this.responseData.productData) {
        this.products = this.responseData.productData;
        this.products.custom = '';
        this.products.checkbox_value = '';
        var i; var l;
        for (i = 0; i < this.products.length; i++) {
          this.products[i].userpic = "https://www.oukazon.com/storage/profile/small_" + this.products[i].image;
          this.products[i].screen_shot = this.products[i].screen_shot.split(",");
          this.products[i].tag = this.products[i].tag.split(",");
          this.products[i].latlong = this.products[i].latlong.split(",");
          this.products[i].lat = this.products[i].latlong[0];
          this.products[i].long = this.products[i].latlong[1];
          this.products[i].pictures = this.products[i].screen_shot;
          this.products[i].screen_shot = "https://www.oukazon.com/storage/products/screenshot/small_" + this.products[i].screen_shot[0];
          this.products[i].custom_types = this.products[i].custom_types.split(",");
          this.products[i].custom_values = this.products[i].custom_values.split(",");
          this.products[i].custom_fields = this.products[i].custom_fields.split(",");
          for (l = 0; l < this.products[i].custom_types.length; l++) {
            if (this.products[i].custom_types[l] !== "checkbox") {
              this.products[i].custom_types[l] = { type: this.products[i].custom_types[l], title: this.products[i].custom_fields[l], value: this.products[i].custom_values[l] };
            } else {
              if (this.products[i].custom_values[l] !== '') { this.products[i].checkbox_title = this.products[i].custom_fields[l]; }
              this.products[i].checkbox_value = this.products[i].custom_values[l];
            }
          }
          if (this.products[i].checkbox_value !== undefined) { this.products[i].checkbox_value = this.products[i].checkbox_value.split("+"); }
          else { this.products[i].checkbox_value = ''; }
          this.products[i].custom = this.products[i].custom_types;
        }
        console.log(this.products);
      } else {
        console.log("no");
      }
    }, (err) => {

    });
  }

  getCompany() {
    this.authService.postData(this.userPostDataComp, "findcompany").then((result) => {
      this.responseDataComp = result;
      if (this.responseDataComp.companyData) {
        this.companies = this.responseDataComp.companyData;
        this.companies.custom = '';
        this.companies.checkbox_value = '';
        var i; var l;
        for (i = 0; i < this.companies.length; i++) {
          this.companies[i].userpic = "https://www.oukazon.com/storage/profile/small_" + this.companies[i].image;
          this.companies[i].screen_shot = this.companies[i].screen_shot.split(",");
          this.companies[i].tag = this.companies[i].tag.split(",");
          this.companies[i].latlong = this.companies[i].latlong.split(",");
          this.companies[i].lat = this.companies[i].latlong[0];
          this.companies[i].long = this.companies[i].latlong[1];
          this.companies[i].pictures = this.companies[i].screen_shot;
          this.companies[i].screen_shot = "https://www.oukazon.com/storage/companies/screenshot/small_" + this.companies[i].screen_shot[0];
          this.companies[i].custom_types = this.companies[i].custom_types.split(",");
          this.companies[i].custom_values = this.companies[i].custom_values.split(",");
          this.companies[i].custom_fields = this.companies[i].custom_fields.split(",");
          for (l = 0; l < this.companies[i].custom_types.length; l++) {
            if (this.companies[i].custom_types[l] !== "checkbox") {
              this.companies[i].custom_types[l] = { type: this.companies[i].custom_types[l], title: this.companies[i].custom_fields[l], value: this.companies[i].custom_values[l] };
            } else {
              if (this.companies[i].custom_values[l] !== '') { this.companies[i].checkbox_title = this.companies[i].custom_fields[l]; }
              this.companies[i].checkbox_value = this.companies[i].custom_values[l];
            }
          }
          if (this.companies[i].checkbox_value !== undefined) { this.companies[i].checkbox_value = this.companies[i].checkbox_value.split("+"); }
          else { this.companies[i].checkbox_value = ''; }
          this.companies[i].custom = this.companies[i].custom_types;
        }
        console.log(this.companies);
      } else {
        console.log("no");
      }
    }, (err) => {

    });
  }


  getQuestion() {
    this.authService.postData(this.userPostDataAsk, "findquestion").then((result) => {
      this.responseDataAsk = result;
      if (this.responseDataAsk.questionData) {
        this.questions = this.responseDataAsk.questionData;
        var i;
        for (i = 0; i < this.questions.length; i++) {
          this.questions[i].userpic = "https://www.oukazon.com/storage/profile/small_" + this.questions[i].image;
          this.questions[i].screen_shot = this.questions[i].screen_shot.split(",");
          this.questions[i].tag = this.questions[i].tag.split(",");
          this.questions[i].latlong = this.questions[i].latlong.split(",");
          this.questions[i].lat = this.questions[i].latlong[0];
          this.questions[i].long = this.questions[i].latlong[1];
          this.questions[i].pictures = this.questions[i].screen_shot;
          this.questions[i].screen_shot = "https://www.oukazon.com/storage/questions/screenshot/small_" + this.questions[i].screen_shot[0];
        }
        console.log(this.questions);
      } else {
        console.log("no");
      }
    }, (err) => {

    });
  }










}
