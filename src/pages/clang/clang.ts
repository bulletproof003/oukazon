import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';
import { Oukazon } from '../../app/app.component';

@Component({
  selector: 'page-clang',
  templateUrl: 'clang.html',
})
export class ClangPage {
  lang : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, private toastCtrl:ToastController) {
    
  }

  setlangAR(){
    this.lang = 'AR';
        localStorage.setItem('clang', JSON.stringify(this.lang));
        this.navCtrl.setRoot(Oukazon);
    }
  setlangEN(){
    this.lang = 'EN';
        localStorage.setItem('clang', JSON.stringify(this.lang));
        this.navCtrl.setRoot(Oukazon);
    }



}
