import { Component } from '@angular/core';
import { NavController, App, PopoverController, ModalController, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Common } from '../../providers/common';
import { Events } from 'ionic-angular';
import { AdDetailPage } from '../ad-detail/ad-detail';
import { CompDetailPage } from '../comp-detail/comp-detail';
import { AskDetailPage } from '../ask-detail/ask-detail';
import { PostyourPage } from '../postyour/postyour';
import { SmodalPage } from '../smodal/smodal';
import { SettingsPage } from '../settings/settings';
import { ClangPage } from '../clang/clang';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  my: string = "ads";
  products: any;
  productsData: any;
  noRecords: boolean;
  companies: any;
  companiesData: any;
  noRecordsComp: boolean;
  questions: any;
  questionsData: any;
  noRecordsAsk: boolean;
  responseData: any;
  responseDataComp: any;
  responseDataAsk: any;
  userDetails: any;
  nbmsgs1: number;
  langData: number;
  userPostData = { 
    id: "", 
    token: "",
    updated_at: "",
    cat_id: "",
    sub_cat_id: "",
  };
  userPostDataComp = { 
    id: "", 
    token: "",
    updated_at: "",
  };
  userPostDataAsk = { 
    id: "", 
    token: "",
    updated_at: "",
  };
  constructor(public alertCtrl: AlertController, public modalCtrl: ModalController, private events: Events, public common: Common, public authService: AuthService, public popoverCtrl: PopoverController, public navCtrl: NavController, public app: App) {
    if (localStorage.getItem('clang')) {
      this.langData = JSON.parse(localStorage.getItem('clang'));
    } else {
      this.navCtrl.setRoot(ClangPage);
    }
      const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
    this.userPostData.id = this.userDetails.id;
    this.userPostData.token = this.userDetails.token;
    this.userPostData.cat_id = '';
    this.userPostData.sub_cat_id = '';
    this.userPostData.updated_at = '';
    this.userPostDataComp.id = this.userDetails.id;
    this.userPostDataComp.token = this.userDetails.token;
    this.userPostDataComp.updated_at = '';
    this.userPostDataAsk.id = this.userDetails.id;
    this.userPostDataAsk.token = this.userDetails.token;
    this.userPostDataAsk.updated_at = '';
    this.noRecords = false;
    this.noRecordsComp = false;
    this.noRecordsAsk = false;
    this.getProduct();
    this.getCompany();
    this.getQuestion();
  }

  doRefresh(refresher) {
    this.userPostData.updated_at = '';
    this.userPostDataComp.updated_at = '';
    this.userPostDataAsk.updated_at = '';
    this.noRecords = false;
    this.noRecordsComp = false;
    this.noRecordsAsk = false;
    this.products = [];
    this.productsData = [];
    this.companies = [];
    this.companiesData = [];
    this.questions = [];
    this.questionsData = [];
    this.getProduct();
    this.getCompany();
    this.getQuestion();
    setTimeout(() => {
      refresher.complete();
    }, 1500);
  }

  openModal() {
    var modalPage = this.navCtrl.push(SmodalPage);
  }

  opensettings() {
    this.navCtrl.push(SettingsPage);
  }

  presentPopover(myEvent) {
    if (this.userDetails.name == "" || this.userDetails.email == "") {
      this.missinginfo();
    } else {
    let popover = this.popoverCtrl.create(PostyourPage);
    popover.present({
      ev: myEvent
    });
    }
  }
  missinginfo() {
    let confirm = this.alertCtrl.create({
      title: 'Posting IN OUKAZON?',
      message: 'Please Update your profile In Order to POST anAD, List a COMPANY or Ask a QUESTION. Happy Oukazonning !',
      buttons: [
        {
          text: 'Later',
          handler: () => {
            this.navCtrl.setRoot(HomePage);
          }
        },
        {
          text: 'UPDATE',
          handler: () => {
            this.navCtrl.push(SettingsPage);
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.events.subscribe('nbmsgs', (nbmsgs, dummyNumber) => {
      console.log(nbmsgs);
     
      this.nbmsgs1 = nbmsgs;
  }); 
  }



  ngOnInit() {
    this.products = [];
    this.companies = [];
    this.questions = [];
  }

  showAd(i) {
    this.navCtrl.push(AdDetailPage, { "product": this.products[i] });
  }
  showComp(i) {
    this.navCtrl.push(CompDetailPage, { "company": this.companies[i] });
  }
  showAsk(i) {
    this.navCtrl.push(AskDetailPage, { "question": this.questions[i] });
  }

  getProduct() {
    this.common.presentLoading();
    this.authService.postData(this.userPostData, "products").then((result) => {
      this.responseData = result;
      if (this.responseData.productData0) {
        this.products = this.responseData.productData0;
        this.userPostData.updated_at = this.products[this.products.length - 1].updated_at;
        console.log("Updated At " + this.userPostData.updated_at);
        this.products.custom = '';
        this.products.checkbox_value = '';
        var i; var l;
        for (i = 0; i < this.products.length; i++) {
          this.products[i].userpic = "https://www.oukazon.com/storage/profile/small_" + this.products[i].image;
          this.products[i].screen_shot = this.products[i].screen_shot.split(",");
          this.products[i].tag = this.products[i].tag.split(",");
          this.products[i].latlong = this.products[i].latlong.split(",");
          this.products[i].lat = this.products[i].latlong[0];
          this.products[i].long = this.products[i].latlong[1];
          this.products[i].pictures = this.products[i].screen_shot;
          this.products[i].screen_shot = "https://www.oukazon.com/storage/products/screenshot/small_" + this.products[i].screen_shot[0];
          this.products[i].custom_types = this.products[i].custom_types.split(",");
          this.products[i].custom_values = this.products[i].custom_values.split(",");
          this.products[i].custom_fields = this.products[i].custom_fields.split(",");
          for (l = 0; l < this.products[i].custom_types.length; l++) {
            if (this.products[i].custom_types[l] !== "checkbox") {
              this.products[i].custom_types[l] = { type: this.products[i].custom_types[l], title: this.products[i].custom_fields[l], value: this.products[i].custom_values[l] };
            } else {
              if (this.products[i].custom_values[l] !== '') { this.products[i].checkbox_title = this.products[i].custom_fields[l]; }
              this.products[i].checkbox_value = this.products[i].custom_values[l];
            }
          }
          if (this.products[i].checkbox_value !== undefined) { this.products[i].checkbox_value = this.products[i].checkbox_value.split("+"); }
          else { this.products[i].checkbox_value = ''; }
          this.products[i].custom = this.products[i].custom_types;
        }
        console.log(this.products);
      } else {
        console.log("no");
      }
    }, (err) => {

    });
    this.common.closeLoading();
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      
      this.authService.postData(this.userPostData, "products").then((result) => {
        this.responseData = result;
        if (this.responseData.productData0.length) {
          this.productsData = this.responseData.productData0;
          this.userPostData.updated_at = this.productsData[this.productsData.length - 1].updated_at;
          var a; var b;
          
          this.productsData.custom = '';
          this.productsData.checkbox_value = '';
          for (a = 0; a < this.productsData.length; a++) {
            this.productsData[a].userpic = "https://www.oukazon.com/storage/profile/small_" + this.productsData[a].image;
            this.productsData[a].screen_shot = this.productsData[a].screen_shot.split(",");
            this.productsData[a].tag = this.productsData[a].tag.split(",");
            this.productsData[a].latlong = this.productsData[a].latlong.split(",");
            this.productsData[a].lat = this.productsData[a].latlong[0];
            this.productsData[a].long = this.productsData[a].latlong[1];
            this.productsData[a].pictures = this.productsData[a].screen_shot;
            this.productsData[a].screen_shot = "https://www.oukazon.com/storage/products/screenshot/small_" + this.productsData[a].screen_shot[0];
            this.productsData[a].custom_types = this.productsData[a].custom_types.split(",");
            this.productsData[a].custom_values = this.productsData[a].custom_values.split(",");
            this.productsData[a].custom_fields = this.productsData[a].custom_fields.split(",");
            for (b = 0; b < this.productsData[a].custom_types.length; b++) {
              if (this.productsData[a].custom_types[b] !== "checkbox") {
                this.productsData[a].custom_types[b] = { type: this.productsData[a].custom_types[b], title: this.productsData[a].custom_fields[b], value: this.productsData[a].custom_values[b] };
              } else {
                if (this.productsData[a].custom_values[b] !== '') { this.productsData[a].checkbox_title = this.productsData[a].custom_fields[b]; }
                this.productsData[a].checkbox_value = this.productsData[a].custom_values[b];
              }
            }
            if (this.productsData[a].checkbox_value !== undefined) { this.productsData[a].checkbox_value = this.productsData[a].checkbox_value.split("+"); }
            else { this.productsData[a].checkbox_value = ''; }
            this.productsData[a].custom = this.productsData[a].custom_types;
          }

          for (let m = 0; m < this.productsData.length; m++) {
            this.products.push(this.productsData[m]);
          }
          console.log(this.products);
        } else {
          this.noRecords = true;
        }
      }, (err) => {
  
      });
      
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  getCompany() {
    this.authService.postData(this.userPostDataComp, "companies").then((result) => {
      this.responseDataComp = result;
      if (this.responseDataComp.companyData0) {
        this.companies = this.responseDataComp.companyData0;
        this.userPostDataComp.updated_at = this.companies[this.companies.length -1].updated_at;
        console.log("Updated At " + this.userPostDataComp.updated_at);
        this.companies.custom = '';
        this.companies.checkbox_value = '';
        var i; var l;
        for (i = 0; i < this.companies.length; i++) {
          this.companies[i].userpic = "https://www.oukazon.com/storage/profile/small_" + this.companies[i].image;
          this.companies[i].screen_shot = this.companies[i].screen_shot.split(",");
          this.companies[i].tag = this.companies[i].tag.split(",");
          this.companies[i].latlong = this.companies[i].latlong.split(",");
          this.companies[i].lat = this.companies[i].latlong[0];
          this.companies[i].long = this.companies[i].latlong[1];
          this.companies[i].pictures = this.companies[i].screen_shot;
          this.companies[i].screen_shot = "https://www.oukazon.com/storage/companies/screenshot/small_" + this.companies[i].screen_shot[0];
          this.companies[i].custom_types = this.companies[i].custom_types.split(",");
          this.companies[i].custom_values = this.companies[i].custom_values.split(",");
          this.companies[i].custom_fields = this.companies[i].custom_fields.split(",");
          for (l = 0; l < this.companies[i].custom_types.length; l++) {
            if (this.companies[i].custom_types[l] !== "checkbox") {
              this.companies[i].custom_types[l] = { type: this.companies[i].custom_types[l], title: this.companies[i].custom_fields[l], value: this.companies[i].custom_values[l] };
            } else {
              if (this.companies[i].custom_values[l] !== '') { this.companies[i].checkbox_title = this.companies[i].custom_fields[l]; }
              this.companies[i].checkbox_value = this.companies[i].custom_values[l];
            }
          }
          if (this.companies[i].checkbox_value !== undefined) { this.companies[i].checkbox_value = this.companies[i].checkbox_value.split("+"); }
          else { this.companies[i].checkbox_value = ''; }
          this.companies[i].custom = this.companies[i].custom_types;
        }
        console.log(this.companies);
      } else {
        console.log("no");
      }
    }, (err) => {

    });
  }

  doInfiniteComp(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      
      this.authService.postData(this.userPostDataComp, "companies").then((result) => {
        this.responseDataComp = result;
        if (this.responseDataComp.companyData0.length) {
          this.companiesData = this.responseDataComp.companyData0;
          this.userPostDataComp.updated_at = this.companiesData[this.companiesData.length - 1].updated_at;
          var a; var b;
          
          this.companiesData.custom = '';
          this.companiesData.checkbox_value = '';
          for (a = 0; a < this.companiesData.length; a++) {
            this.companiesData[a].userpic = "https://www.oukazon.com/storage/profile/small_" + this.companiesData[a].image;
            this.companiesData[a].screen_shot = this.companiesData[a].screen_shot.split(",");
            this.companiesData[a].tag = this.companiesData[a].tag.split(",");
            this.companiesData[a].latlong = this.companiesData[a].latlong.split(",");
            this.companiesData[a].lat = this.companiesData[a].latlong[0];
            this.companiesData[a].long = this.companiesData[a].latlong[1];
            this.companiesData[a].pictures = this.companiesData[a].screen_shot;
            this.companiesData[a].screen_shot = "https://www.oukazon.com/storage/companies/screenshot/small_" + this.companiesData[a].screen_shot[0];
            this.companiesData[a].custom_types = this.companiesData[a].custom_types.split(",");
            this.companiesData[a].custom_values = this.companiesData[a].custom_values.split(",");
            this.companiesData[a].custom_fields = this.companiesData[a].custom_fields.split(",");
            for (b = 0; b < this.companiesData[a].custom_types.length; b++) {
              if (this.companiesData[a].custom_types[b] !== "checkbox") {
                this.companiesData[a].custom_types[b] = { type: this.companiesData[a].custom_types[b], title: this.companiesData[a].custom_fields[b], value: this.companiesData[a].custom_values[b] };
              } else {
                if (this.companiesData[a].custom_values[b] !== '') { this.companiesData[a].checkbox_title = this.companiesData[a].custom_fields[b]; }
                this.companiesData[a].checkbox_value = this.companiesData[a].custom_values[b];
              }
            }
            if (this.companiesData[a].checkbox_value !== undefined) { this.companiesData[a].checkbox_value = this.companiesData[a].checkbox_value.split("+"); }
            else { this.companiesData[a].checkbox_value = ''; }
            this.companiesData[a].custom = this.companiesData[a].custom_types;
          }

          for (let m = 0; m < this.companiesData.length; m++) {
            this.companies.push(this.companiesData[m]);
          }
          console.log(this.companies);
        } else {
          this.noRecordsComp = true;
        }
      }, (err) => {
  
      });
      
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  getQuestion() {
    this.authService.postData(this.userPostDataAsk, "questions").then((result) => {
      this.responseDataAsk = result;
      if (this.responseDataAsk.questionData0) {
        this.questions = this.responseDataAsk.questionData0;
        this.userPostDataAsk.updated_at = this.questions[this.questions.length - 1].updated_at;
        console.log("Updated At " + this.userPostDataAsk.updated_at);
        var i;
        for (i = 0; i < this.questions.length; i++) {
          this.questions[i].userpic = "https://www.oukazon.com/storage/profile/small_" + this.questions[i].image;
          this.questions[i].screen_shot = this.questions[i].screen_shot.split(",");
          this.questions[i].tag = this.questions[i].tag.split(",");
          this.questions[i].latlong = this.questions[i].latlong.split(",");
          this.questions[i].lat = this.questions[i].latlong[0];
          this.questions[i].long = this.questions[i].latlong[1];
          this.questions[i].pictures = this.questions[i].screen_shot;
          this.questions[i].screen_shot = "https://www.oukazon.com/storage/questions/screenshot/small_" + this.questions[i].screen_shot[0];
        }
        console.log(this.questions);
      } else {
        console.log("no");
      }
    }, (err) => {

    });
  }

  doInfiniteAsk(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.authService.postData(this.userPostDataAsk, "questions").then((result) => {
        this.responseDataAsk = result;
        if (this.responseDataAsk.questionData0.length) {
          this.questionsData = this.responseDataAsk.questionData0;
          this.userPostDataAsk.updated_at = this.questionsData[this.questionsData.length - 1].updated_at;
          var a;
          for (a = 0; a < this.questionsData.length; a++) {
            this.questionsData[a].userpic = "https://www.oukazon.com/storage/profile/small_" + this.questionsData[a].image;
            this.questionsData[a].screen_shot = this.questionsData[a].screen_shot.split(",");
            this.questionsData[a].tag = this.questionsData[a].tag.split(",");
            this.questionsData[a].latlong = this.questionsData[a].latlong.split(",");
            this.questionsData[a].lat = this.questionsData[a].latlong[0];
            this.questionsData[a].long = this.questionsData[a].latlong[1];
            this.questionsData[a].pictures = this.questionsData[a].screen_shot;
            this.questionsData[a].screen_shot = "https://www.oukazon.com/storage/questions/screenshot/small_" + this.questionsData[a].screen_shot[0];
          }

          for (let m = 0; m < this.questionsData.length; m++) {
            this.questions.push(this.questionsData[m]);
          }
          console.log(this.questions);
        } else {
          this.noRecordsAsk = true;
        }
      }, (err) => {
  
      });
      
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }




}
